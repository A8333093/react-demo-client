import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, compose, createStore, Middleware, StoreEnhancer } from 'redux';

import sagas from '../sagas';
import reducer from '../reducers';
import Config from '../config';
import history from '../services/history';

const enhancers: StoreEnhancer<object>[] = [];
const middlewares: Middleware[] = [];

// start: Chrome redux devtools extension enable
const { reduxDevtoolsExtension } = Config.chrome;
declare const window: Window & { __REDUX_DEVTOOLS_EXTENSION__: Function };
if (reduxDevtoolsExtension.enabled && window.__REDUX_DEVTOOLS_EXTENSION__) {
  enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}
// end: Chrome redux devtools extension enable

// Save saga middleware
const sagaMiddleware = createSagaMiddleware();
middlewares.push(sagaMiddleware);

// Compose enhancers
const composedEnhancers = compose(applyMiddleware(...middlewares), ...enhancers);

// define store Creator
export default (persistedState: any = {}) => {
  const store = createStore(reducer, persistedState, composedEnhancers);

  // Run sagas
  sagaMiddleware.run(sagas, store.dispatch.bind(store), history);

  return store;
};
