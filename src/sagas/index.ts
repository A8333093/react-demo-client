import { History } from 'history';
import { Dispatch } from 'react-redux';
import { SagaIterator } from 'redux-saga';
import { all, fork } from 'redux-saga/effects';

import * as Ajax from '../modules/ajax';
import * as Auth from '../modules/auth';
import * as Records from '../modules/records';
import * as Navigate from '../modules/navigate';
import * as Categories from '../modules/categories';
import { takeEveryAction } from '../modules/common';

const redirects = function* (): SagaIterator {
  const {
    login: { success: loginSuccess },
    signup: { success: signupSuccess },
    resetPassword: { success: resetPasswordSuccess },
    logout,
  } = Auth.Actions;

  const goHome = Navigate.redirect('/');
  const goLogin = Navigate.redirect('/login');
  const goRecords = Navigate.redirect('/records');

  yield all([
    // after signup redirect to login page
    takeEveryAction(signupSuccess, goLogin),

    // after login redirect to user records page
    takeEveryAction(loginSuccess, goRecords),

    // after logout redirect to home page
    takeEveryAction(logout, goHome),

    // after reset password redirect to login page
    takeEveryAction(resetPasswordSuccess, goLogin),
  ]);
};

export default function* saga(dispatch: Dispatch<any>, history: History): SagaIterator {
  yield all([
    fork(redirects),
    fork(Navigate.saga, dispatch, history),
    fork(Ajax.saga),
    fork(Auth.saga),
    fork(Records.saga),
    fork(Categories.saga),
  ]);
}
