import { RouteProps } from 'react-router-dom';

import MainPage from './components/pages/HomePage';
import LoginPage from './components/pages/auth/LoginPage';
import NotFoundPage from './components/pages/NotFoundPage';
import SignUpPage from './components/pages/auth/SignupPage';
import RecordsPage from './components/pages/records/RecordsPage';
import ActivationPage from './components/pages/auth/ActivationPage';
import CategoriesPage from './components/pages/categories/CategoriesPage';
import PasswordResetPage from './components/pages/auth/ResetPasswordPage';
import PasswordForgotPage from './components/pages/auth/ForgotPasswordPage';

export const routes: RouteProps[] = [{
  path: '/',
  exact: true,
  component: MainPage,
}, {
  path: '/records',
  component: RecordsPage,
}, {
  path: '/categories',
  component: CategoriesPage,
}, {
  path: '/login',
  component: LoginPage,
}, {
  path: '/signup',
  component: SignUpPage,
}, {
  path: '/password-forgot',
  component: PasswordForgotPage,
}, {
  path: '/password-reset/:token',
  component: PasswordResetPage,
}, {
  path: '/activate/:token',
  component: ActivationPage,
}, {
  path: '/*',
  component: NotFoundPage,
}];