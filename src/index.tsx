import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { routes } from './routes';
import createStore from './store';
import App from './components/App';
import './styles/index.css';

import 'toastr/build/toastr.min.css';
import 'semantic-ui-css/semantic.min.css';

render(
  <Provider store={createStore()}>
    <App routes={routes} />
  </Provider>,
  document.getElementById('root')
);