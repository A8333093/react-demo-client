import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

declare interface Global extends NodeJS.Global {
  requestAnimationFrame: (cb: () => void) => void;
}

const raf = (global as Global).requestAnimationFrame = (cb) => {
  setTimeout(cb, 0);
};

export default raf;