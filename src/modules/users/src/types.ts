import { User } from '..';

type Id = string;

// Payloads
export namespace Payloads {
  export type Save = User;

  export type Fetch = undefined;

  export type Remove = Id;
}

// Results
export namespace Results {
  export type Save = User;

  export type Fetch = User[];

  export type Remove = undefined;
}