import { DbDocument } from '../../common';

export interface User extends DbDocument {
  email: string;
  profile: {
    local: {
      firstName: string;
      lastName: string;
      isActivated: boolean;
      password: string;
    };
  };
}

export const getFullUserName = (user?: User) =>
  user && user.email && user.profile.local
  && `${user.profile.local.firstName} ${user.profile.local.lastName}`;