import { History } from 'history';
import { Dispatch } from 'react-redux';
import { SagaIterator } from 'redux-saga';
import { all, call, fork, put } from 'redux-saga/effects';

import { Actions } from '..';
import { ActionCreator, switcher, takeAction } from '../../common';

export const redirectEffect =
  (pathname: string) =>
    put(Actions.go(pathname));

export const redirect =
  (pathname: string) =>
    function* (): SagaIterator {
      yield redirectEffect(pathname);
    };

const watchNavigate =
  function* (history: History) {
    while (true) {
      const { payload } = yield takeAction(Actions.go);
      yield call(history.push, payload);
    }
  };

export const saga =
  function* (dispatch: Dispatch<any>, history: History): SagaIterator {
    const getActionCreator = (action: string) =>
      switcher<ActionCreator<any>, string>()
        .case('POP', () => Actions.pop)
        .case('PUSH', () => Actions.push)
        .case('REPLACE', () => Actions.replace)
        .run(action)
        .get();

    history.listen(({ pathname }, action) => {
      dispatch(getActionCreator(action)(pathname));
    });

    const effects = [
      fork(watchNavigate, history),
      put(Actions.init(location.pathname))
    ];

    yield all(effects);
  };