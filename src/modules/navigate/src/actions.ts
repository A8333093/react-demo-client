import { moduleId } from '..';
import { actionCreatorFactory } from '../../common';

// Types
const GO = 'GO';
const POP = 'POP';
const INIT = 'INIT';
const PUSH = 'PUSH';
const REPLACE = 'REPLACE';

// Payloads
export type NavigatePayload = string;

// Actions
const ac = actionCreatorFactory(`@@${moduleId.toUpperCase()}`);
const go = ac<NavigatePayload>(GO);
const pop = ac<NavigatePayload>(POP);
const init = ac<NavigatePayload>(INIT);
const push = ac<NavigatePayload>(PUSH);
const replace = ac<NavigatePayload>(REPLACE);

export const Actions = {
  go,
  pop,
  init,
  push,
  replace,
};