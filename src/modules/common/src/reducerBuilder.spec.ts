import { actionCreatorFactory } from './actionFactory';
import { reducerBuilder } from './reducerBuilder';

const actionFactory = actionCreatorFactory('@@TEST');
const toUpperCaseActionCreator = actionFactory('TO_UPPER_CASE');
const toLowerCaseActionCreator = actionFactory('TO_LOWER_CASE');
type SubstrPayload = { from: number, length?: number };
const substrActionCreator = actionFactory<SubstrPayload>('SUBSTR');

interface State {
  greeting: string;
  date?: string;
}

const initialState: State = ({ greeting: 'hello' });
const unknownAction = actionFactory('UNKNOWN')();
const toUpperCaseHandler = ({ greeting }: State, payload: undefined, meta: any) => {
  const state: State = { greeting: greeting.toUpperCase() };

  if (meta && meta.date) {
    state.date = meta.date;
  }

  return state;
};
const toLowerCaseHandler = ({ greeting }: State) => ({ greeting: greeting.toLocaleLowerCase() });
const substrHandler =
  ({ greeting }: State, { from, length }: SubstrPayload) =>
    ({ greeting: greeting.substr(from, length) });

// Testing
describe('TSReducer', () => {
  it('should return not changed state if onNo have cases', () => {
    const reducer = reducerBuilder<State>();

    expect(reducer(initialState, unknownAction)).toEqual(initialState);
  });
  it('should return not changed state on unknown action', () => {
    const reducer = reducerBuilder<State>()
      .case(toUpperCaseActionCreator, toUpperCaseHandler)
      .case(toLowerCaseActionCreator, toLowerCaseHandler)
      .case(substrActionCreator, substrHandler);

    expect(reducer(initialState, unknownAction)).toEqual(initialState);
  });
  it('should return initial state on unknown action and undefined state', () => {
    const reducer = reducerBuilder(initialState);

    expect(reducer(undefined as any, unknownAction)).toEqual(initialState);
  });
  it('should return correct state on known action', () => {
    const date = new Date().toDateString();
    const reducer = reducerBuilder()
      .case(substrActionCreator, substrHandler)
      .case(toUpperCaseActionCreator, toUpperCaseHandler);

    expect(reducer(initialState, substrActionCreator({ from: 2 }))).toEqual({ greeting: 'llo' });
    expect(reducer(initialState, toUpperCaseActionCreator(undefined, { date })))
      .toEqual({ greeting: 'HELLO', date });
  });
});