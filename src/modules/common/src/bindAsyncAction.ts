import { delay, SagaIterator } from 'redux-saga';
import { call, cancelled, put, take, takeEvery } from 'redux-saga/effects';

import { Action, ActionCreator, AnyAction, AsyncActionCreator } from '..';

export const bindAsyncAction =
  <P, R>(asyncActionCreator: AsyncActionCreator<P, R>) =>
    (sagaWorker: (payload: P, ...args: any[]) => Promise<R> | SagaIterator,
     before?: (...args: any[]) => any,
     after?: (...args: any[]) => any) =>
      function* (...args: any[]): SagaIterator {
        const payload: P = args[0];

        if (before) {
          yield (call as any)(before, payload, ...args);
        }

        yield put(asyncActionCreator.request(payload));

        try {

          const result = yield (call as any)(sagaWorker, payload, ...args);
          yield put(asyncActionCreator.success({ payload, result }));
          return result;

        } catch (error) {

          yield put(asyncActionCreator.failure({ payload, error: error.message }));

        } finally {

          if (yield cancelled()) {
            yield put(asyncActionCreator.failure({ payload, error: 'Request was cancelled' }));
          }
          if (after) {
            yield (call as any)(after, payload, ...args);
          }

        }
      };

export const delayAction =
  (ms: number, action: AnyAction) =>
    function* (): SagaIterator {
      yield call(delay, ms);
      yield put(action);
    };

export const takeAction =
  (actionCreator: ActionCreator<any> | ActionCreator<any>[]) =>
    take(
      Array.isArray(actionCreator)
        ? actionCreator.map(({ type }) => type)
        : actionCreator.type
    );

export const takeEveryAction =
  <P>(actionCreator: ActionCreator<P> | ActionCreator<P>[],
      worker: (action: Action<P>, ...args: any[]) => SagaIterator | Promise<P>,
      ...args: any[]) =>
    (takeEvery as any)(
      Array.isArray(actionCreator)
        ? actionCreator.map(({ type }) => type)
        : actionCreator.type,
      worker,
      ...args
    );

export const logAction =
  function* <P>(action: Action<P>): SagaIterator {
    const { log } = console;
    yield call(log, { action });
  };