// Types
import { actionCreatorFactory } from './actionFactory';

// Mock Helpers
const getPrefix = () => '@@TEST';

const getActionType = () => 'SOME_ACTION';
const getAsyncActionType = () => 'SOME_ASYNC_ACTION';
const getAsyncActionTypeRequest = () => 'SOME_ASYNC_ACTION_REQUEST';
const getAsyncActionTypeSuccess = () => 'SOME_ASYNC_ACTION_SUCCESS';
const getAsyncActionTypeFailure = () => 'SOME_ASYNC_ACTION_FAILURE';

const getFactoryMeta = () => ({ factory: 'some_factory_meta' });
const getCreatorMeta = () => ({ creator: 'some_creator_meta' });
const getActionMeta = () => ({ action: 'some_action_meta' });

const getActionPayload = () => 'Action Payload';
const getAsyncActionPayload = () => 'Async Action Payload';
const getAsyncActionRequestPayload = () => 'Async Action Request Payload';
const getAsyncActionSuccessPayload = () => 'Async Action Success Payload';
const getAsyncActionFailurePayload = () => 'Async Action Failure Payload';

const getAsyncActionSuccessResult = () => 'Async Action Success Result';
const getAsyncActionFailureError = () => 'Async Action Failure Error';

const getActionCreatorFactory = () =>
  actionCreatorFactory(getPrefix(), getFactoryMeta());

// Test
describe('TSAction Testing', () => {
  describe('Action Factory Testing', () => {
    it('The factory is callable and have property "async", which is also callable', () => {
      const actionFactory = getActionCreatorFactory();

      expect(typeof actionFactory).toBe('function');
      expect(actionFactory).toHaveProperty('async');
      expect(typeof actionFactory.async).toBe('function');
    });
    it('The factory sends the correct prefix and meta data', () => {
      const actionFactoryMeta = getFactoryMeta();
      const actionTypePrefix = getPrefix();
      const actionFactory = actionCreatorFactory(actionTypePrefix, actionFactoryMeta);

      const actionType = getActionType();
      const actionCreator = actionFactory<string>(actionType);

      const action = actionCreator();

      const actionFullType = `${actionTypePrefix}/${actionType}`;

      // testing

      expect(actionCreator).toHaveProperty('type');
      expect(actionCreator.type).toEqual(actionFullType);
      expect(action.meta).toEqual(getFactoryMeta());
    });
  });
  describe('Action Creator Testing', () => {
    it('Action Creator have properties: "type": string and "match": function', () => {
      const actionFactory = actionCreatorFactory();
      const actionType = getActionType();
      const actionCreator = actionFactory<string>(actionType);

      expect(actionCreator).toHaveProperty('type');
      expect(typeof actionCreator.type).toBe('string');

      expect(actionCreator).toHaveProperty('match');
      expect(typeof actionCreator.match).toBe('function');
    });
    it('Action Creator sends the correct type and meta data', () => {
      const actionFactory = actionCreatorFactory();

      const actionType = getActionType();
      const actionCreatorMeta = getCreatorMeta();
      const actionCreator = actionFactory<string>(actionType, actionCreatorMeta);

      const action = actionCreator();

      // testing

      expect(action).toHaveProperty('type');
      expect(action.type).toBe(actionType);

      expect(action).toHaveProperty('meta');
      expect(action.meta).toEqual(actionCreatorMeta);
    });
    it('Action Creator create the correct Actions', () => {
      const actionFactory = actionCreatorFactory();

      const actionType = getActionType();
      const actionCreatorMeta = getCreatorMeta();
      const actionCreator = actionFactory<string>(actionType, actionCreatorMeta);

      const actionPayload = getActionPayload();
      const actionMeta = getActionMeta();
      const actionError = true;
      const action = actionCreator(actionPayload, actionMeta, actionError);

      const actionMergedMeta = { ...actionMeta, ...actionCreatorMeta };

      // testing

      ['type', 'payload', 'meta', 'error'].map(property => {
        expect(action).toHaveProperty(property);
      });

      expect(action.type).toEqual(actionType);
      expect(action.payload).toEqual(actionPayload);
      expect(action.meta).toEqual(actionMergedMeta);
      expect(action.error).toEqual(actionError);
    });
  });
  describe('Async Action Creator Testing', () => {
    it('Async Action Creator have properties: "type": string and "match": function', () => {
      const actionFactory = actionCreatorFactory();
      const asyncActionType = getAsyncActionType();
      const asyncActionCreator = actionFactory.async<string, string>(asyncActionType);

      expect(asyncActionCreator).toHaveProperty('type');
      expect(typeof asyncActionCreator.type).toBe('string');

      expect(asyncActionCreator).toHaveProperty('match');
      expect(typeof asyncActionCreator.match).toBe('function');
    });
    it('Async Action Creator sends the correct types and meta data', () => {
      const actionFactory = actionCreatorFactory();

      const actionType = getAsyncActionType();
      const actionCreatorMeta = getCreatorMeta();
      const asyncActionCreator = actionFactory.async<string, string>(actionType, actionCreatorMeta);

      const asyncAction = asyncActionCreator();
      const asyncActionRequest = asyncActionCreator.request();
      const asyncActionSuccess = asyncActionCreator.success();
      const asyncActionFailure = asyncActionCreator.failure();

      const actionTypeRequest = getAsyncActionTypeRequest();
      const actionTypeSuccess = getAsyncActionTypeSuccess();
      const actionTypeFailure = getAsyncActionTypeFailure();

      // testing

      expect(asyncAction).toHaveProperty('type');
      expect(asyncAction.type).toBe(actionType);
      expect(asyncAction).toHaveProperty('meta');
      expect(asyncAction.meta).toEqual(actionCreatorMeta);

      expect(asyncActionRequest).toHaveProperty('type');
      expect(asyncActionRequest.type).toBe(actionTypeRequest);
      expect(asyncActionRequest).toHaveProperty('meta');
      expect(asyncActionRequest.meta).toEqual(actionCreatorMeta);

      expect(asyncActionSuccess).toHaveProperty('type');
      expect(asyncActionSuccess.type).toBe(actionTypeSuccess);
      expect(asyncActionSuccess).toHaveProperty('meta');
      expect(asyncActionSuccess.meta).toEqual(actionCreatorMeta);

      expect(asyncActionFailure).toHaveProperty('type');
      expect(asyncActionFailure.type).toBe(actionTypeFailure);
      expect(asyncActionFailure).toHaveProperty('meta');
      expect(asyncActionFailure.meta).toEqual(actionCreatorMeta);
    });
    it('Async Action Creator create the correct Actions', () => {
      const actionFactory = actionCreatorFactory();

      const asyncActionType = getAsyncActionType();
      const asyncActionCreatorMeta = getCreatorMeta();
      const asyncActionCreator = actionFactory.async<string, string>(asyncActionType, asyncActionCreatorMeta);

      const asyncActionMeta = getActionMeta();

      const asyncActionPayload = getAsyncActionPayload();
      const asyncActionError = false;
      const asyncAction =
        asyncActionCreator(asyncActionPayload, asyncActionMeta, asyncActionError);

      const asyncActionRequestPayload = getAsyncActionRequestPayload();
      const asyncActionRequestError = false;
      const asyncActionRequest =
        asyncActionCreator.request(asyncActionRequestPayload, asyncActionMeta, asyncActionRequestError);

      const asyncActionSuccessPayload = getAsyncActionSuccessPayload();
      const asyncActionSuccessResult = getAsyncActionSuccessResult();
      const asyncActionSuccessError = false;
      const asyncActionSuccess =
        asyncActionCreator.success(
          { payload: asyncActionSuccessPayload, result: asyncActionSuccessResult },
          asyncActionMeta,
          asyncActionSuccessError
        );

      const asyncActionFailurePayload = getAsyncActionFailurePayload();
      const asyncActionFailurePayloadError = getAsyncActionFailureError();
      const asyncActionFailureError = true;
      const asyncActionFailure =
        asyncActionCreator.failure(
          { payload: asyncActionFailurePayload, error: asyncActionFailurePayloadError },
          asyncActionMeta,
          asyncActionFailureError
        );

      const asyncActionMergedMeta = { ...asyncActionMeta, ...asyncActionCreatorMeta };

      const asyncActionTypeRequest = getAsyncActionTypeRequest();
      const asyncActionTypeSuccess = getAsyncActionTypeSuccess();
      const asyncActionTypeFailure = getAsyncActionTypeFailure();
      const asyncActionSuccessPayloadMerged = {
        payload: asyncActionSuccessPayload,
        result: asyncActionSuccessResult
      };
      const asyncActionErrorPayloadMerged = {
        payload: asyncActionFailurePayload,
        error: asyncActionFailurePayloadError
      };

      // testing

      // actions have properties
      [asyncAction, asyncActionRequest, asyncActionSuccess, asyncActionFailure].map(action => {
        ['type', 'payload', 'meta', 'error'].map(property => {
          expect(action).toHaveProperty(property);
        });

        // actions have the correct meta data
        expect(action.meta).toEqual(asyncActionMergedMeta);
      });

      // actions have the correct types
      expect(asyncAction.type).toEqual(asyncActionType);
      expect(asyncActionRequest.type).toEqual(asyncActionTypeRequest);
      expect(asyncActionSuccess.type).toEqual(asyncActionTypeSuccess);
      expect(asyncActionFailure.type).toEqual(asyncActionTypeFailure);

      // actions have the correct payloads
      expect(asyncAction.payload).toEqual(asyncActionPayload);
      expect(asyncActionRequest.payload).toEqual(asyncActionRequestPayload);
      expect(asyncActionSuccess.payload).toEqual(asyncActionSuccessPayloadMerged);
      expect(asyncActionFailure.payload).toEqual(asyncActionErrorPayloadMerged);

      // actions have the correct errors
      expect(asyncAction.error).toBeFalsy();
      expect(asyncActionRequest.error).toBeFalsy();
      expect(asyncActionSuccess.error).toBeFalsy();
      expect(asyncActionFailure.error).toBeTruthy();
    });
  });
  describe('Actions Testing', () => {
    it('Actions types match Action Creators', () => {
      const actionFactory = actionCreatorFactory();

      const actionType = getActionType();
      const actionCreator = actionFactory(actionType);

      const asyncActionType = getAsyncActionType();
      const asyncActionCreatorMeta = getCreatorMeta();
      const asyncActionCreator = actionFactory.async<string, string>(asyncActionType, asyncActionCreatorMeta);

      const action = actionCreator();
      const asyncAction = asyncActionCreator();
      const asyncActionRequest = asyncActionCreator.request();
      const asyncActionSuccess = asyncActionCreator.success();
      const asyncActionFailure = asyncActionCreator.failure();

      // testing

      // match
      expect(actionCreator.match(action)).toBeTruthy();
      expect(asyncActionCreator.match(asyncAction)).toBeTruthy();
      expect(asyncActionCreator.request.match(asyncActionRequest)).toBeTruthy();
      expect(asyncActionCreator.success.match(asyncActionSuccess)).toBeTruthy();
      expect(asyncActionCreator.failure.match(asyncActionFailure)).toBeTruthy();

      // not match

      expect(actionCreator.match(asyncActionFailure)).toBeFalsy();
      expect(asyncActionCreator.match(action)).toBeFalsy();
      expect(asyncActionCreator.request.match(asyncAction)).toBeFalsy();
      expect(asyncActionCreator.success.match(asyncActionRequest)).toBeFalsy();
      expect(asyncActionCreator.failure.match(asyncActionSuccess)).toBeFalsy();
    });
  });
});