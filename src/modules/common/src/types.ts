export interface DbDocument {
  __v: number;
  _id: string;
}