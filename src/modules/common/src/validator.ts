export interface Validator {
  (key: string, value: string, data: any): string;
}

export type Errors<T> = Partial<Record<keyof T, string>>;

export const isEmpty = (o: string) => !o || o.length === 0;

export const isEmail = (email: string) =>
  /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(email);

export const getErrors = <T>(data: T, validator: Validator): Errors<T> => {
  let errors: Errors<T> = {} as any;

  Object.keys(data)
    .forEach(key => {
      const error = validator(key, data[key], data);
      if (error && error.length > 0) {
        errors[key] = error;
      }
    });

  return errors;
};

export const Validator = {
  isEmpty,
  isEmail,
  getErrors,
};