import { Action, Dispatch, Middleware, MiddlewareAPI } from 'redux';

const { log } = console;

interface ExtendedMiddleware<T> extends Middleware {
  <S extends T>(api: MiddlewareAPI<S>): (next: Dispatch<S>) => Dispatch<S>;
}

interface StatedMiddleware extends ExtendedMiddleware<object> {
}

export const callbackMiddleware =
  (cb: (action: Action, state: object, prevState: object) => void): StatedMiddleware =>
    <S extends object>(api: MiddlewareAPI<S>) =>
      (next: Dispatch<S>) =>
        <A extends Action>(action: A): A => {
          const prevState = api.getState();
          const nextAction = next(action);
          const state = api.getState();

          cb(action, state, prevState);

          return nextAction;
        };

export const loggerMiddleware: StatedMiddleware =
  callbackMiddleware((action, state, prevState) => log('LoggerMiddleware:', {
    action, state: state, prevState
  }));