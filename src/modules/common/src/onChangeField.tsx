import { FormEvent } from 'react';

export const onChangeField =
  (onChange: (name: string, value: string) => any) =>
    (e: FormEvent<any>) => {
      onChange(e.currentTarget.name, e.currentTarget.value);
    };