export const log: typeof console.log = (...args: any[]) => {
  console.log.apply(null, args);
  return args[0];
};

export const dir = console.dir;