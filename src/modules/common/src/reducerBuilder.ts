import { ActionCreator, AnyAction } from '..';

export interface Handler<S, P> {
  (state: S, payload: P, meta: any, error: boolean): S;
}

interface Case<S, P> {
  actionCreator: ActionCreator<P>;
  handler: Handler<S, P>;
}

export const reducerBuilder = <S>(initialState?: S) => {
  const cases: Case<S, any>[] = [];

  const reducer = Object.assign(
    (state: S = initialState!, action: AnyAction) => {
      for (const { actionCreator, handler } of cases) {
        if (actionCreator.match(action)) {
          state = Object.assign({}, handler(state, action.payload, action.meta, action.error!));
        }
      }
      return state;
    },
    {
      case: <P>(actionCreator: ActionCreator<P>, handler: Handler<S, P>) => {
        if (actionCreator && handler) {
          cases.push({ actionCreator, handler });
        }
        return reducer;
      },
    }
  );
  return reducer;
};