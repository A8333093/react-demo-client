import { FormEvent } from 'react';

export const preventDefault =
  <T1, T2, T3>(func?: (a1?: T1, a2?: T2, a3?: T3) => any, a1?: T1, a2?: T2, a3?: T3) =>
    (e: FormEvent<any>, ...args: any[]) => {
      e.preventDefault();

      if (func) {
        func.call(null, a1, a2, a3);
      }
    };