/**
 * Definition
 */

export interface AnyAction {
  type: any;
}

export type Meta = undefined | { [key: string]: any };

export interface Action<P> extends AnyAction {
  type: string;
  payload?: P;
  meta?: Meta;
  error?: boolean;
}

export interface Success<P, R> {
  payload: P;
  result: R;
}

export interface Failure<P, E = string> {
  payload: P;
  error: E;
}

export interface ActionCreator<P> {
  type: string;

  match: (action: AnyAction) => action is Action<P>;

  <T extends P>(payload?: T, meta?: Meta, error?: boolean): Action<T>;
}

export interface AsyncActionCreator<P, R, E = string> extends ActionCreator<P> {
  request: ActionCreator<P>;
  success: ActionCreator<Success<P, R>>;
  failure: ActionCreator<Failure<P, E>>;
}

/**
 * Implementation
 */

// Subtypes
const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';

// helpers
const typeHelper =
  (type: string, prefix?: string) =>
    (prefix && prefix.length ? `${prefix}/${type}` : type).toUpperCase();

const isEmptyObject = (o: any) =>
  (o === undefined) ||
  (o === null) ||
  (typeof o === 'object' && Object.keys(o).length === 0);

const asyncTypeHelper = (type: string, prefix?: string, action?: string) =>
  action ? `${typeHelper(type, prefix)}_${action}` : typeHelper(type, prefix);

const metaHelper = (meta?: Meta, creatorMeta?: Meta, factoryMeta?: Meta) =>
  !isEmptyObject(factoryMeta) || !isEmptyObject(creatorMeta) || !isEmptyObject(meta)
    ? Object.assign({}, factoryMeta, creatorMeta, meta)
    : undefined;

const matchHelper = <P>(actionCreator: ActionCreator<P>) =>
  (action: AnyAction): action is Action<P> =>
    actionCreator.type === action.type;

const actionCreatorHelper = <T>(type: string, creatorFunction: Function): T => {
  const creator: any = Object.assign(creatorFunction, { type });
  creator.match = matchHelper(creator as any);
  creator.toString = () => type;
  return creator;
};

const actionHelper =
  <P>(type: string, payload?: P, meta?: Meta, error?: boolean): Action<P> => {
    const action: any = { type };

    if (!isEmptyObject(payload)) {
      const p: any = payload;
      if (isEmptyObject(p.payload)) {
        delete p.payload;
      }
      if (isEmptyObject(p.result)) {
        delete p.result;
      }
      if (isEmptyObject(p.error)) {
        delete p.error;
      }
      if (!isEmptyObject(p)) {
        action.payload = p;
      }
    }

    if (!isEmptyObject(meta)) {
      action.meta = meta;
    }

    if (error !== undefined) {
      action.error = error;
    }

    return action;
  };

// implementation
export const actionCreatorFactory =
  (prefix: string = '', factoryMeta: Meta = {}) => Object.assign(
    <P>(type: string, creatorMeta: Meta = {}) =>
      actionCreatorHelper<ActionCreator<P>>(
        typeHelper(type, prefix),
        (payload?: P, meta?: Meta, error?: boolean): Action<P> =>
          actionHelper(
            typeHelper(type, prefix),
            payload,
            metaHelper(meta, creatorMeta, factoryMeta),
            error
          )
      ),
    {
      async: <P, R, E = string>(type: string, creatorMeta: Meta = {}) => Object.assign(
        actionCreatorHelper<ActionCreator<P>>(
          typeHelper(type, prefix),
          (payload?: P, meta?: Meta, error?: boolean): Action<P> =>
            actionHelper(
              typeHelper(type, prefix),
              payload,
              metaHelper(meta, creatorMeta, factoryMeta),
              error
            )
        ),
        {
          request: actionCreatorHelper<ActionCreator<P>>(
            asyncTypeHelper(type, prefix, REQUEST),
            (payload?: P, meta?: Meta, error?: boolean): Action<P> =>
              actionHelper(
                asyncTypeHelper(type, prefix, REQUEST),
                payload,
                metaHelper(meta, creatorMeta, factoryMeta),
                error
              )
          ),
          success: actionCreatorHelper<ActionCreator<Success<P, R>>>(
            asyncTypeHelper(type, prefix, SUCCESS),
            (payload?: Success<P, R>, meta?: Meta, error?: boolean): Action<Success<P, R>> =>
              actionHelper(
                asyncTypeHelper(type, prefix, SUCCESS),
                payload,
                metaHelper(meta, creatorMeta, factoryMeta),
                error
              )
          ),
          failure: actionCreatorHelper<ActionCreator<Failure<P, E>>>(
            asyncTypeHelper(type, prefix, FAILURE),
            (payload?: Failure<P, E>, meta?: Meta, error?: boolean): Action<Failure<P, E>> =>
              actionHelper(
                asyncTypeHelper(type, prefix, FAILURE),
                payload,
                metaHelper(meta, creatorMeta, factoryMeta),
                error
              )
          )
        }
      ) as AsyncActionCreator<P, R, E>
    }
  );