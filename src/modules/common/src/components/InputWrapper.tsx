import * as React from 'react';
import { Icon, Message } from 'semantic-ui-react';

interface Props {
  [key: string]: any;

  name?: string;
  value?: any;
  error?: string;
  as: React.StatelessComponent<any>;
  onChange: (name: string, value: any) => any;
}

type State = {
  value: any;
};

export class InputWrapper extends React.Component<Props, State> {
  private onChange = ((e: React.SyntheticEvent<any>, target: { value: any }) => {
    e.preventDefault();

    this.props.onChange(this.props.name || '', target.value);

    this.setState({ value: target.value });
  });

  constructor(props: Props) {
    super(props);

    this.state = { value: props.value || '' };
  }

  render() {
    const { onChange } = this;
    const { value } = this.state;

    const { value: v, onChange: oc, as: Input, error, ...props } = this.props;

    let noop: any = { v, oc };
    noop = noop = null;

    return (
      <div>
        <Input {...{ value, onChange, ...props }} />

        {error && (
          <Message size="tiny" color="red">
            <Icon name="remove" corner={true} /> {error}
          </Message>
        )}

        <i />
      </div>
    );
  }
}