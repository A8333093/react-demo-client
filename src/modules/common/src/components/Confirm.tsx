import * as React from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import { preventDefault } from '../preventDefault';

interface Props {
  title?: string;
  message?: string;
  onNo: () => any;
  onYes: () => any;
  open: boolean;
}

export const Confirm: React.SFC<Props> = ({ open, title, message, onNo, onYes }) => (
  <Modal open={open} basic={true} size="small" onClose={onNo}>
    <Header icon="warning" content={title || 'Confirmation required'} />
    <Modal.Content>
      <p>{message || 'Are you sure?'}</p>
    </Modal.Content>
    <Modal.Actions>
      <Button basic={true} color="red" inverted={true} onClick={preventDefault(onNo)}>
        <Icon name="remove" /> No
      </Button>
      <Button color="green" inverted={true} onClick={preventDefault(onYes)}>
        <Icon name="checkmark" /> Yes
      </Button>
    </Modal.Actions>
  </Modal>
);