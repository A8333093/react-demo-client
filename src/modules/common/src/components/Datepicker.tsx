import * as React from 'react';
import * as Moment from 'moment';
import DatePickerBase from 'react-datepicker';
import { Divider, Icon, Message } from 'semantic-ui-react';

import 'react-datepicker/dist/react-datepicker.css';

interface Props {
  [key: string]: any;

  name?: string;
  selected?: string;
  error?: string;
  onChange: (name: string, value: any) => any;
}

type State = {
  selected: Moment.Moment;
};

export class DatePicker extends React.Component<Props, State> {
  private onChange = ((selected: Moment.Moment) => {
    this.props.onChange(this.props.name || '', selected.toISOString());

    this.setState({ selected });
  });

  constructor(props: Props) {
    super(props);

    this.state = {
      selected: Moment(
        props.selected
          ? new Date(props.selected)
          : new Date())
    };
  }

  render() {
    const { onChange } = this;
    const { selected } = this.state;

    const { value: v, onChange: o, error, selected: s, ...props } = this.props;

    let noop: any = { v, o, s };
    noop = noop = null;

    return (
      <div>
        <DatePickerBase {...{ selected, onChange, ...props }} />

        {error && (
          <Message size="tiny" color="red">
            <Icon name="remove" corner={true} /> {error}
          </Message>
        )}

        <Divider />
      </div>
    );
  }
}