interface Expression<S, C> {
  (state: S): C;
}

interface Handler<S, C> {
  (state: S, _case: C): S;
}

interface Case<S, C> {
  expression: Expression<S, C>;
  handler: Handler<S, C>;
}

class Switcher<S, C> {
  private _state: S;
  private _cases: Case<S, C>[] = [];
  private _val = (<V extends (S | C)>(value: any) => (): V => value);
  private _isFunction = ((func: any): func is Function => func.apply && func.call);

  static val = <V>(value: V) => () => value;

  constructor(init?: S) {
    this._state = init!;
  }

  set(expression: Expression<S, S> | S) {
    this._state = this._isFunction(expression) ? expression(this._state) : expression;

    return this;
  }

  case(expression: Expression<S, C> | C, handler: Handler<S, C> | S) {
    this._cases.push({
      expression: this._isFunction(expression) ? expression : this._val(expression),
      handler: this._isFunction(handler) ? handler : this._val(handler),
    });

    return this;
  }

  cases(expressions: (Expression<S, C> | C)[], handler: Handler<S, C> | S) {
    for (const expression of expressions) {
      this.case(expression, handler);
    }

    return this;
  }

  run(expressions: Expression<S, C> | C) {
    const value = this._isFunction(expressions) ? expressions(this._state) : expressions;

    for (const _case of this._cases) {
      if (value === _case.expression(this._state)) {
        this._state = _case.handler(this._state, value);
      }
    }

    return this;
  }

  get(): S {
    return this._state;
  }
}

export const switcher = <S, V>(init?: S) => new Switcher<S, V>(init);