import * as React from 'react';
import { connect } from 'react-redux';
import { Dimmer as BaseDimmer, Header, Icon } from 'semantic-ui-react';

import { selector } from '../..';

interface Props {
  active: boolean;
}

const Component: React.SFC<Props> = ({ active }) => (
  <BaseDimmer active={active} page={true}>
    <Header as="h2" icon={true} inverted={true}>
      <Icon name="spinner" loading={true} size="massive" />
    </Header>
  </BaseDimmer>
);

const mapStateToProps = (state: any) => ({
  active: selector(state).callsInProgress
});

export const Dimmer = connect(mapStateToProps)(Component);
