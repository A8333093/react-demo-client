export interface AjaxRequest {
  url: string;
  method: Method;
  data?: any;
}

export interface AjaxResponse {
  data: any;
  status: number;
  statusText: string;
}

export interface AjaxError {
  status?: number;
  message: string;
}

export enum Method {
  GET = 'GET',
  PUT = 'PUT',
  POST = 'POST',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}