import { reducerBuilder } from '../../common';

import { Actions as A, State } from '..';

const INITIAL_STATE: State = {
  calls: 0,
  callsInProgress: false,
};

export const reducer = reducerBuilder(INITIAL_STATE)
  .case(A.ajax.request, ({ calls }) => ({ calls: ++calls, callsInProgress: calls !== 0 }))
  .case(A.ajax.success, ({ calls }) => ({ calls: --calls, callsInProgress: calls !== 0 }))
  .case(A.ajax.failure, ({ calls }) => ({ calls: --calls, callsInProgress: calls !== 0 }));