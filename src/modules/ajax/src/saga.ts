import Axios from 'axios';
import { SagaIterator } from 'redux-saga';
import { all, call, cancelled, fork, put, take } from 'redux-saga/effects';

import Config from '../../../config';
import { TokenApi } from '../../auth';
import { Actions, AjaxRequest, Method } from '..';

const getRequestOption = () => ({
  headers: {
    pragma: 'onNo-cache',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${TokenApi.getToken()}`
  },
  validateStatus: (status: any) => true,
  credentials: 'same-origin'
});

const getQueryString = (params: any = {}): string => {
  return Object.keys(params)
    .map(key => `?${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');
};

const axiosRequest = ({ url, method, data }: AjaxRequest) => {
  if (!url) {
    throw new Error('Url must be specified');
  }

  url = `${Config.api.url}${url}`;

  switch (method) {
    case Method.GET:
      return Axios.get(`${url}${getQueryString(data)}`, getRequestOption());
    case Method.PUT:
      return Axios.put(url, JSON.stringify(data), getRequestOption());
    case Method.POST:
      return Axios.post(url, JSON.stringify(data), getRequestOption());
    case Method.PATCH:
      return Axios.patch(url, JSON.stringify(data), getRequestOption());
    case Method.DELETE:
      return Axios.delete(url, getRequestOption());
    default:
      throw new Error(`Wrong method '${method}'`);
  }
};

const request = function* (payload: any, meta: any): SagaIterator {
  // put request action
  yield put(Actions.ajax.request(payload, meta));

  try {
    // process ajax action
    const response = yield call(axiosRequest, payload);

    // get response
    const { data: { data, message }, status, statusText } = response;

    // status ok
    if (status >= 200 && status < 300 && data) {
      yield put(Actions.ajax.success(
        { payload, result: { data, status, statusText } }, meta
      ));
    } else if (status >= 400) {
      yield put(Actions.ajax.failure(
        { payload, error: { status, message: message || statusText } }, meta
      ));
    }
  } catch (e) {
    if (e.response) {
      const { data, status, statusText } = e.response;
      const error = {
        status,
        message: (data.message ? data.message : statusText) || e.message
      };
      yield put(Actions.ajax.failure({ payload, error }, meta));
    } else {
      const error = { message: e.message };
      yield put(Actions.ajax.failure({ payload, error }, meta));
    }
  } finally {
    if (yield cancelled()) {
      const message = 'Request was cancelled';
      yield put(Actions.ajax.failure({ payload, error: { message } }, meta));
    }
  }
};

const ajax = function* (): SagaIterator {
  while (true) {
    const { payload, meta } = yield take(Actions.ajax.type);

    yield fork(request, payload, meta);
  }
};

export const saga = function* (): SagaIterator {
  yield all([
    fork(ajax),
  ]);
};