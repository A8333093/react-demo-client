import { actionCreatorFactory } from '../../common';
import { AjaxError, AjaxRequest, AjaxResponse, Method, moduleId } from '..';

// Types
const AJAX = 'AJAX';

// Actions
const ac = actionCreatorFactory(`@@${moduleId.toUpperCase()}`);
export const ajax = ac.async<AjaxRequest, AjaxResponse, AjaxError>(AJAX);

// Helpers
export const get = (url: string, data?: any) => ajax({ url, data, method: Method.GET });
export const put = (url: string, data?: any) => ajax({ url, data, method: Method.PUT });
export const post = (url: string, data?: any) => ajax({ url, data, method: Method.POST });
export const patch = (url: string, data?: any) => ajax({ url, data, method: Method.PATCH });
export const Delete = (url: string, data?: any) => ajax({ url, data, method: Method.DELETE });

export const Actions = {
  ajax,
};
