export interface State {
  calls: number;
  callsInProgress: boolean;
}
