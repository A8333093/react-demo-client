import { SagaIterator } from 'redux-saga';
import { call, put } from 'redux-saga/effects';

import { Actions as A, AjaxRequest } from '..';
import { Action, AsyncActionCreator, Failure, Success, takeAction } from '../../common';

const nextRequestId = ((seed: number) => () => seed++)(0);

export const bindAjaxAction =
  <P, R, E>(asyncActionCreator: AsyncActionCreator<P, R, E>) =>
    (ajaxActionCreator: (...args: any[]) => Action<AjaxRequest>,
     onSuccess?: (payload: Success<P, R>) => any,
     onFailure?: (payload: Failure<P, E>) => any) =>
      function* (...args: any[]): SagaIterator {
        const payload = (args && args[0] && args[0].payload) ? args[0].payload : undefined;

        // put request action
        yield put(asyncActionCreator.request(payload));

        const ajaxAction = yield (call as any)(ajaxActionCreator, payload);
        const requestId = nextRequestId();

        // mix meta with requestId
        yield put({ ...ajaxAction, meta: { requestId } });

        // await success or failure action with meta with requestId
        let response = yield takeAction([A.ajax.success, A.ajax.failure]);
        while (!response.meta || requestId !== response.meta.requestId) {
          response = yield takeAction([A.ajax.success, A.ajax.failure]);
        }

        if (A.ajax.failure.match(response)) {
          const error: E = response.payload!.error.message as any;
          if (onFailure) {
            yield call(onFailure, { payload, error });
          }
          yield put(asyncActionCreator.failure({ payload, error }));
        } else {
          const result = response.payload.result.data;
          if (onSuccess) {
            yield call(onSuccess, { payload, result });
          }
          yield put(asyncActionCreator.success({ payload, result }));
        }
      };