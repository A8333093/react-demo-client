import * as React from 'react';
import { Form, Segment } from 'semantic-ui-react';

import { Category } from '../models';
import { Errors, InputWrapper, preventDefault } from '../../../common';

interface Props {
  errors: Errors<Category>;
  category: Category;
  onChange: (name: string, value: string) => any;
}

export const SaveForm: React.SFC<Props> = ({ category, errors, onChange }) => (
  <Segment>
    <Form size="large" onSubmit={preventDefault()}>
      <InputWrapper
        as={Form.Input}
        name="title"
        value={category.title}
        placeholder="Title"

        onChange={onChange}
        error={errors.title}
      />

      <InputWrapper
        as={Form.Input}
        name="description"
        value={category.description}
        placeholder="Description"

        onChange={onChange}
        error={errors.description}
      />
    </Form>
  </Segment>
);