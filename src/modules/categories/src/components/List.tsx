import * as React from 'react';
import { Table } from 'semantic-ui-react';

import { Category, ListRow } from '../..';

interface Props {
  categories: Category[];
  onEdit: (category: Category) => any;
  onRemove: (category: Category) => any;
}

export const List: React.SFC<Props> = ({ categories, onEdit, onRemove }) => (
  <Table celled={true}>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Title</Table.HeaderCell>
        <Table.HeaderCell>Description</Table.HeaderCell>
        <Table.HeaderCell width={2} />
        <Table.HeaderCell width={2} />
      </Table.Row>
    </Table.Header>

    {categories && categories.length > 0 && (
      <Table.Body>
        {categories.map(category => (
          <ListRow key={category._id} {...{ category, onEdit, onRemove }} />
        ))}
      </Table.Body>
    )}
  </Table>
);