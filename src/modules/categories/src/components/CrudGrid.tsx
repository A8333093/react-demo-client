import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Button, Container } from 'semantic-ui-react';

import { Actions, Category, List, SaveModal, selector } from '../..';
import { Confirm, Errors, getErrors, isEmpty, preventDefault } from '../../../common';

interface Props {
  categories: Category[];
  fetch: () => any;
  save: (category: Category) => any;
  remove: (id: string) => any;
}

type State = {
  toSave?: Partial<Category>;
  toRemove?: Category;
  errors: Errors<Category>;
};

class Component extends React.Component<Props, State> {
  state = {
    toSave: undefined as any as Category,
    toRemove: undefined as any as Category,
    errors: {},
  };

  private onEdit = ((toSave: Category) => this.setState({ toSave }));

  private onRemove = ((toRemove: Category) => this.setState({ toRemove }));

  private onNo = (() => this.setState({ toRemove: undefined }));

  private onYes = (() => {
    const { props: { remove }, state: { toRemove } } = this;

    if (toRemove) {
      remove(toRemove._id);
    }

    this.setState({ toRemove: undefined });
  });

  private onAdd = (() => {
    this.setState({
      toSave: {
        title: '',
        description: '',
      }
    });
  });

  private onChange = ((name: string, value: string) => {
    this.state.toSave[name] = value;
  });

  private onCancel = (() => this.setState({
    toSave: undefined,
    errors: {},
  }));

  private onSave = (() => {
    const { props: { save }, state: { toSave }, formIsValid } = this;

    if (toSave && formIsValid()) {
      save(toSave);
      this.setState({
        toSave: undefined,
        errors: {},
      });
    }
  });

  private formIsValid = (() => {
    const { validator, state: { toSave } } = this;
    const errors = getErrors(toSave, validator);

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  });

  private validator = ((key: string, value: string) => {
    if (key === 'title') {
      if (isEmpty(value)) {
        return 'Title is required.';
      }
      if (value.length < 3) {
        return 'Title too short.';
      }
    }
    if (key === 'description') {
      if (isEmpty(value)) {
        return 'Description is required.';
      }
    }
    return '';
  });

  componentWillMount() {
    this.props.fetch();
  }

  render() {
    const { categories } = this.props;
    const { toSave, toRemove, errors } = this.state;
    const { onEdit, onRemove, onNo, onYes, onAdd, onChange, onCancel, onSave } = this;

    return (
      <Container>
        <Button onClick={preventDefault(onAdd)}>
          Add new Category
        </Button>

        <List {...{ categories, onEdit, onRemove }} />

        {!!toSave && (
          <SaveModal
            open={!!toSave}
            category={toSave}
            errors={errors}
            {...{ onChange, onCancel, onSave }}
          />
        )}

        {!!toRemove && (
          <Confirm
            open={!!toRemove}
            {...{ onNo, onYes }}
            title="Are you sure?"
            message={`Remove category "${toRemove.title}" ?`}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  categories: selector(state).list,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  fetch: () => dispatch(Actions.fetch()),
  save: (category: Category) => dispatch(Actions.save(category)),
  remove: (id: string) => dispatch(Actions.remove(id)),
});

export const GrudGrid = connect(mapStateToProps, mapDispatchToProps)(Component);