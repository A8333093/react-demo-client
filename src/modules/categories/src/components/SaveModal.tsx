import * as React from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';

import { Category, SaveForm } from '../..';
import { Errors, preventDefault } from '../../../common';

interface Props {
  open: boolean;
  category: Category;
  onChange: (name: string, value: string) => any;
  errors: Errors<Category>;
  onSave: () => any;
  onCancel: () => any;
}

export const SaveModal: React.SFC<Props> = ({ open, category, onChange, errors, onCancel, onSave }) => (
  <Modal open={open} onClose={onCancel} basic={true} size="small">
    <Header icon="add" content={`${category._id ? 'Edit' : 'Add new'} category`} />
    <Modal.Content>
      <SaveForm category={category} onChange={onChange} errors={errors} />
    </Modal.Content>
    <Modal.Actions>
      <Button basic={true} color="red" inverted={true} onClick={preventDefault(onCancel)}>
        <Icon name="remove" /> Cancel
      </Button>
      <Button color="green" inverted={true} onClick={preventDefault(onSave)}>
        <Icon name="checkmark" /> Save
      </Button>
    </Modal.Actions>
  </Modal>
);