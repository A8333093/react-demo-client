import * as React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Table } from 'semantic-ui-react';

import { Category } from '../..';
import { preventDefault } from '../../../common';

interface Props {
  category: Category;
  onEdit: (category: Category) => any;
  onRemove: (category: Category) => any;
}

export const ListRow: React.SFC<Props> = ({ category, onEdit, onRemove }) => {
  const { title, description } = category;
  return (
    <Table.Row>
      <Table.Cell>{title}</Table.Cell>
      <Table.Cell>{description}</Table.Cell>
      <Table.Cell>
        <Link to="" onClick={preventDefault(onEdit, category)}>
          <Icon name="edit" /> Edit
        </Link>
      </Table.Cell>
      <Table.Cell>
        <Link to="" onClick={preventDefault(onRemove, category)}>
          <Icon name="remove" /> Delete
        </Link>
      </Table.Cell>
    </Table.Row>
  );
};