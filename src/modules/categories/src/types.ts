import { Category } from '..';

type Id = string;

// Payloads
export namespace Payloads {
  export type Save = Category;

  export type Fetch = undefined;

  export type Remove = Id;
}

// Results
export namespace Results {
  export type Save = Category;

  export type Fetch = Category[];

  export type Remove = undefined;
}