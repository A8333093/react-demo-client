import { Delete, get, post } from '../../ajax';
import { Category } from './models';

const save = (category: Category) => post('/category', { category });
const fetch = () => get('/categories');
const remove = (id: string) => Delete(`/category/${id}`);

export const sagaApi = {
  save,
  fetch,
  remove,
};