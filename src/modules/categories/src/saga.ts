import * as Toastr from 'toastr';
import { all } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { bindAjaxAction } from '../../ajax';
import { Success, takeEveryAction } from '../../common';
import { Actions as A, sagaApi as Api } from '..';

const save = bindAjaxAction(A.save)(
  Api.save,
  ({ payload }: Success<any, any>) => {
    if (payload._id) {
      Toastr.success('Category was updated successfully!');
    } else {
      Toastr.success('Category was created successfully!');
    }
  }
);

const fetch = bindAjaxAction(A.fetch)(Api.fetch);

const remove = bindAjaxAction(A.remove)(
  Api.remove,
  () => Toastr.success('Category was deleted successfully!')
);

export const saga = function* (): SagaIterator {
  yield all([
    takeEveryAction(A.save, save),
    takeEveryAction(A.fetch, fetch),
    takeEveryAction(A.remove, remove),
  ]);
};