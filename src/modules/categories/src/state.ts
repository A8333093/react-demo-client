import { Category } from '..';

export interface State {
  list?: Category[];
  current?: Category;
}