import { DbDocument } from '../../common';

export interface Category extends DbDocument {
  title: string;
  userId: string;
  description: string;
}
