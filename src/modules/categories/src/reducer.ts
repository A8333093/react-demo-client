import { reducerBuilder } from '../../common';
import { Actions as A, Category, State } from '..';

const INITIAL_STATE: State = {
  list: [],
  current: undefined,
};

const remove = (id: string, list: Category[] = []): Category[] =>
  list.filter(category => category._id !== id);

const save = (category: Category, list: Category[] = []): Category[] => {
  let isNew = true;
  list.map(_category => {
    if (_category._id === category._id) {
      isNew = false;
      _category = category;
    }

    return _category;
  });

  if (isNew) {
    return [...list, category];
  }

  return list;
};

export const reducer = reducerBuilder(INITIAL_STATE)
  .case(A.fetch.success, (state, { result: list }) => ({ ...state, list }))
  .case(A.save.success, (state, { result: category }) => ({ ...state, list: save(category, state.list) }))
  .case(A.remove.success, (state, { payload: id }) => ({ ...state, list: remove(id, state.list) }));