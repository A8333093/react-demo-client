import { actionCreatorFactory } from '../../common';
import { moduleId, Payloads as P, Results as R } from '..';

// Types
const SAVE = 'SAVE';
const FETCH = 'FETCH';
const REMOVE = 'REMOVE';

// Actions
const ac = actionCreatorFactory(`@@${moduleId.toUpperCase()}`);
const save = ac.async<P.Save, R.Save>(SAVE);
const fetch = ac.async<P.Fetch, R.Fetch>(FETCH);
const remove = ac.async<P.Remove, R.Remove>(REMOVE);

export const Actions = {
  save,
  fetch,
  remove,
};