import { moduleId, Payloads as P, Results as R } from '..';
import { actionCreatorFactory } from '../../common';

// Types
const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';
const SIGNUP = 'SIGNUP';
const ACTIVATE = 'ACTIVATE';
const CURRENT_USER = 'CURRENT_USER';
const RESET_PASSWORD = 'RESET_PASSWORD';
const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
const CHECK_RESET_TOKEN = 'CHECK_RESET_TOKEN';
const CHECK_AUTHORIZATION = 'CHECK_AUTHORIZATION';

// Actions
const ac = actionCreatorFactory(`@@${moduleId.toUpperCase()}`);
const login = ac.async<P.Login, R.Login>(LOGIN);
const logout = ac(LOGOUT);
const signup = ac.async<P.Signup, R.Signup>(SIGNUP);
const activate = ac.async<P.Activate, R.Activate>(ACTIVATE);
const currentUser = ac.async<P.CurrentUser, R.CurrentUser>(CURRENT_USER);
const resetPassword = ac.async<P.ResetPassword, R.ResetPassword>(RESET_PASSWORD);
const forgotPassword = ac.async<P.ForgotPassword, R.ForgotPassword>(FORGOT_PASSWORD);
const checkResetToken = ac.async<P.CheckResetToken, R.CheckResetToken>(CHECK_RESET_TOKEN);
const checkAuthorization = ac.async<P.CheckAuthorization, R.CheckAuthorization>(CHECK_AUTHORIZATION);

export const Actions = {
  login,
  logout,
  signup,
  activate,
  currentUser,
  resetPassword,
  forgotPassword,
  checkResetToken,
  checkAuthorization
};