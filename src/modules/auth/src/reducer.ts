import { Actions as A, State } from '..';
import { reducerBuilder } from '../../common';

const INITIAL_STATE: State = {
  user: undefined,
  activateResult: undefined,
  checkResetTokenResult: undefined,
};

export const reducer = reducerBuilder(INITIAL_STATE)
  .case(A.login.success, (state, { result: { user } }) => ({ ...state, user }))
  .case(A.logout, state => ({ ...state, user: undefined }))
  .case(A.activate.success, (state, { result }) => ({ ...state, activateResult: result }))
  .case(A.currentUser.success, (state, { result: user }) => ({ ...state, user }))
  .case(A.checkResetToken.success, (state, { result }) => ({ ...state, checkResetTokenResult: result }));
