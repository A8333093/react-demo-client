import * as Toastr from 'toastr';
import { SagaIterator } from 'redux-saga';
import { all, call, fork, put, select } from 'redux-saga/effects';

import * as Navigate from '../../navigate';
import { bindAjaxAction } from '../../ajax';
import { Actions as A, Payloads, sagaApi, selector, TokenApi } from '..';
import { Action, Failure, Success, takeAction, takeEveryAction } from '../../common';

const login = bindAjaxAction(A.login)(
  sagaApi.login,
  (success: Success<any, any>) => {
    if (success.result && success.result.token) {
      TokenApi.saveToken(success.result.token);
      Toastr.success('Login success');
    }
  },
  (failure: Failure<any>) => {
    TokenApi.cleanToken();
    Toastr.error(failure.error);
  }
);

const successMessage = (success: Success<any, any>) => {
  if (success.result && success.result.message) {
    Toastr.success(success.result.message);
  }
};

const failureMessage = (failure: Failure<any>) => {
  if (failure.error) {
    Toastr.error(failure.error);
  }
};

const logout = function* (): SagaIterator {
  yield call(TokenApi.cleanToken);
  yield call(Toastr.success, 'Logout Success');
};

const signup = bindAjaxAction(A.signup)(
  sagaApi.signup,
  successMessage,
  failureMessage
);

const activate = bindAjaxAction(A.activate)(
  sagaApi.activate,
  (success: Success<any, any>) => {
    if (success.result && success.result.status !== 'error') {
      successMessage(success);
    } else {
      failureMessage({ error: success.result } as any);
    }
  },
);

const currentUser = bindAjaxAction(A.currentUser)(sagaApi.currentUser);

const resetPassword = bindAjaxAction(A.resetPassword)(
  sagaApi.resetPassword,
  successMessage,
  failureMessage
);

const forgotPassword = bindAjaxAction(A.forgotPassword)(
  sagaApi.forgotPassword,
  successMessage,
  failureMessage
);

const checkResetToken = bindAjaxAction(A.checkResetToken)(
  sagaApi.checkResetToken,
  successMessage,
  failureMessage
);

const checkAuthorization = function* (action: Action<any>): SagaIterator {
  const payload: Payloads.CheckAuthorization = action.payload!;

  // request action
  yield put(A.checkAuthorization.request(payload));

  // try fetch user from state
  const { user } = yield select(selector);

  // if user already fetched put success
  if (user && user.email) {
    // success
    yield put(A.checkAuthorization.success({ payload, result: user }));
    if (payload.goOnSuccess) {
      yield put(Navigate.Actions.go(payload.goOnSuccess));
    }
  } else {
    // else fetch user
    yield fork(currentUser);

    // await success or failure
    const resultAction = yield takeAction([A.currentUser.success, A.currentUser.failure]);

    if (A.currentUser.success.match(resultAction)) {
      // success
      yield put(A.checkAuthorization.success({ payload, result: resultAction.payload!.result }));
      if (payload.goOnSuccess) {
        yield put(Navigate.Actions.go(payload.goOnSuccess));
      }
    } else {
      // failure
      yield put(A.checkAuthorization.failure({ payload, error: resultAction.payload.error }));
      if (payload.goLoginOnFailure === true) {
        yield put(Navigate.Actions.go('/login'));
      } else if (payload.goOnFailure) {
        yield put(Navigate.Actions.go(payload.goOnFailure));
      }
    }
  }
};

export const saga = function* (): SagaIterator {
  yield all([
    takeEveryAction(A.login, login),
    takeEveryAction(A.logout, logout),
    takeEveryAction(A.signup, signup),
    takeEveryAction(A.activate, activate),
    takeEveryAction(A.currentUser, currentUser),
    takeEveryAction(A.resetPassword, resetPassword),
    takeEveryAction(A.forgotPassword, forgotPassword),
    takeEveryAction(A.checkResetToken, checkResetToken),
    takeEveryAction(A.checkAuthorization, checkAuthorization),
  ]);
};