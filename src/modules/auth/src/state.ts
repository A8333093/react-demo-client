import { User } from '../../users';
import { Results } from '..';

export type State = {
  user?: User;
  activateResult?: Results.Activate;
  checkResetTokenResult?: Results.CheckResetToken;
};