import { User } from '../../users';

// Payloads
export namespace Payloads {
  export type Login = {
    email: string;
    password: string;
  };

  export type Signup = {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
  };

  export type Activate = string;

  export type CurrentUser = undefined;

  export type ResetPassword = {
    email: string;
    token: string;
    password: string;
    confirmPassword: string;
  };

  export type ForgotPassword = string;

  export type CheckResetToken = string;

  export type CheckAuthorization = {
    goOnSuccess?: string;
    goOnFailure?: string;
    goLoginOnFailure?: boolean;
  };
}

export namespace Results {
  export type Login = {
    token: string;
    user: User;
  };

  export type Signup = {
    message: string;
  };

  export type Activate = {
    message: string;
    status: string;
  };

  export type CurrentUser = User;

  export type ResetPassword = {
    message: string;
  };

  export type ForgotPassword = {
    message: string;
  };

  export type CheckResetToken = {
    email: string;
    token: string;
  };

  export type CheckAuthorization = User;
}