import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Button, Form, Segment } from 'semantic-ui-react';

import { Actions, Payloads } from '../..';
import { Errors, getErrors, isEmail, isEmpty, preventDefault, InputWrapper } from '../../../common';

interface Props {
  login: (payload: Payloads.Login) => any;
}

type User = {
  email: string;
  password: string;
};

type State = {
  user: User;
  errors: Errors<User>;
};

class Component extends React.Component<Props, State> {
  state: State = {
    user: {
      email: '',
      password: ''
    },
    errors: {}
  };

  private onChange = ((key: string, value: string) => {
    this.state.user[key] = value;
  });

  private login = (() => {
    const { formIsValid, props: { login }, state: { user } } = this;

    if (formIsValid()) {
      login(user);
    }
  });

  private formIsValid = (() => {
    const { validator, state: { user } } = this;
    const errors = getErrors(user, validator);

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  });

  private validator = ((key: string, value: string) => {
    if (key === 'email') {
      if (isEmpty(value)) {
        return 'Email is required.';
      } else if (!isEmail(value)) {
        return 'Email is not valid.';
      }
    } else if (key === 'password') {
      if (isEmpty(value)) {
        return 'Password is required.';
      }
    }
    return '';
  });

  render() {
    const { onChange, login, state: { user, errors } } = this;

    return (
      <Segment>
        <Form
          size="large"
          onSubmit={preventDefault(login)}
        >
          <InputWrapper
            as={Form.Input}

            icon="user"
            iconPosition="left"

            type="email"
            name="email"
            value={user.email}
            placeholder="Email address"

            onChange={onChange}
            error={errors.email}
          />

          <InputWrapper
            as={Form.Input}

            icon="lock"
            iconPosition="left"

            type="password"
            name="password"
            value={user.password}
            placeholder="Password"

            onChange={onChange}
            error={errors.password}
          />

          <Button
            color="teal"
            fluid={true}
            size="large"
            onClick={preventDefault(login)}
          >
            Login
          </Button>
        </Form>
      </Segment>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  login: (payload: Payloads.Login) => dispatch(Actions.login(payload))
});

export const LoginForm = connect(null, mapDispatchToProps)(Component);