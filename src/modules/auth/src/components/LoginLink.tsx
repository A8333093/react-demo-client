import * as React from 'react';
import { Link } from 'react-router-dom';

export const LoginLink: React.SFC = ({ children, ...props }) => (
  <Link {...props} to="/login">{children}</Link>
);