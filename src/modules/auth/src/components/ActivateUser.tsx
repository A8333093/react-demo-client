import * as React from 'react';
import { Link } from 'react-router-dom';
import { Message } from 'semantic-ui-react';
import { connect, Dispatch } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';

import { Actions, Results, selector } from '../..';

interface Props extends RouteComponentProps<any> {
  activateResult: Results.Activate;
  activate: (token: string) => any;
}

class Component extends React.Component<Props> {
  componentWillMount() {
    const { activate, match: { params } } = this.props;

    activate(params.token);
  }

  render() {
    if (!this.props.activateResult) {
      return null;
    }

    const { message, status } = this.props.activateResult;

    const messageProps = {
      error: status === 'error',
      success: status === 'success',
    };

    return (
      <div>
        {message && <Message {...messageProps}>{message}</Message>}

        <Message>
          Already have an account? <Link to="/login">Login</Link>
        </Message>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  activateResult: selector(state).activateResult
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  activate: (token: string) => dispatch(Actions.activate(token))
});

export const ActivateUser = withRouter(connect(mapStateToProps, mapDispatchToProps)(Component));