import * as React from 'react';
import { connect } from 'react-redux';

import * as Auth from '../..';

interface Props {
  user: any;
}

class Component extends React.Component<Props> {
  private isAuthenticated = (() => {
    const { user } = this.props;
    return user && Object.keys(user).length !== 0;
  });

  render() {
    const { isAuthenticated, props: { children } } = this;
    if (!isAuthenticated()) {
      return null;
    }

    return (
      <div>
        {children}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: Auth.selector(state).user
});

export const AuthorizedBlock = connect(mapStateToProps)(Component);