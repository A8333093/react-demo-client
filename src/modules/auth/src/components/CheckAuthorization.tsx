import * as React from 'react';
import { connect, Dispatch } from 'react-redux';

import { Actions, Payloads } from '../..';

interface Props extends Payloads.CheckAuthorization {
  checkAuthorization: (payload: Payloads.CheckAuthorization) => any;
}

class Component extends React.Component<Props> {
  componentWillMount() {
    const { checkAuthorization, goOnSuccess, goOnFailure } = this.props;
    checkAuthorization({ goOnSuccess, goOnFailure });
  }

  render() {
    return null;
  }
}

const mapDispatchToState = (dispatch: Dispatch<any>) => ({
  checkAuthorization: (payload: Payloads.CheckAuthorization) => dispatch(Actions.checkAuthorization(payload))
});

export const CheckAuthorization = connect(null, mapDispatchToState)(Component);