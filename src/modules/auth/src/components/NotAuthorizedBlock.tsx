import * as React from 'react';
import { connect } from 'react-redux';

import { selector } from '../..';
import { User } from '../../../users';

interface Props {
  user: User;
}

class Component extends React.Component<Props> {
  defaultProps = { user: null };

  private isAuthenticated = (() => {
    const { user } = this.props;
    return user && Object.keys(user).length !== 0;
  });

  render() {
    const { isAuthenticated, props: { children, user } } = this;

    if (user === null || isAuthenticated()) {
      return null;
    }

    return (
      <div>
        {children}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: selector(state).user
});

export const NotAuthorizedBlock = connect(mapStateToProps)(Component);