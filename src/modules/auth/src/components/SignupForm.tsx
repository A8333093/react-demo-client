import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Button, Form, Segment } from 'semantic-ui-react';

import { Actions, Payloads } from '../..';
import { Errors, getErrors, InputWrapper, isEmail, isEmpty, preventDefault } from '../../../common';

interface Props {
  signup: (payload: Payloads.Signup) => any;
}

type User = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
};

type State = {
  user: User;
  errors: Errors<User>;
};

class Component extends React.Component<Props, State> {
  state: State = {
    user: {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: ''
    },
    errors: {}
  };

  private onChange = ((key: string, value: string) => {
    this.state.user[key] = value;
  });

  private signup = (() => {
    const { formIsValid, props: { signup }, state: { user } } = this;

    if (formIsValid()) {
      signup(user);
    }
  });

  private formIsValid = (() => {
    const { validator, state: { user } } = this;
    const errors = getErrors(user, validator);

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  });

  private validator = ((key: string, value: string, data: any) => {
    if (key === 'firstName') {
      if (isEmpty(value)) {
        return 'First Name field is required.';
      }

    } else if (key === 'lastName') {
      if (isEmpty(value)) {
        return 'Last Name field is required.';
      }

    } else if (key === 'email') {
      if (isEmpty(value)) {
        return 'Email field is required.';
      } else if (!isEmail(value)) {
        return 'Email is not valid.';
      }

    } else if (key === 'password') {
      if (isEmpty(value)) {
        return 'Password field is required.';
      }

    } else if (key === 'confirmPassword') {
      if (isEmpty(value)) {
        return 'Please confirm the password.';
      } else if (data.password && data.password !== value) {
        return 'Wrong password.';
      }
    }
    return '';
  });

  render() {
    const { onChange, signup } = this;
    const { errors, user } = this.state;
    const { firstName, lastName, email, password, confirmPassword } = user;

    return (
      <Segment>
        <Form size="large" onSubmit={preventDefault(signup)}>
          <InputWrapper
            as={Form.Input}

            icon="user"
            iconPosition="left"

            name="firstName"
            value={firstName}
            placeholder="First Name"

            onChange={onChange}
            error={errors.firstName}
          />

          <InputWrapper
            as={Form.Input}

            icon="user"
            iconPosition="left"

            name="lastName"
            value={lastName}
            placeholder="Last Name"

            onChange={onChange}
            error={errors.lastName}
          />

          <InputWrapper
            as={Form.Input}

            icon="envelope"
            iconPosition="left"

            type="email"
            name="email"
            value={email}
            placeholder="Email"

            onChange={onChange}
            error={errors.email}
          />

          <InputWrapper
            as={Form.Input}

            icon="lock"
            iconPosition="left"

            type="password"
            name="password"
            value={password}
            placeholder="Password"

            onChange={onChange}
            error={errors.password}
          />

          <InputWrapper
            as={Form.Input}
            icon="lock"
            iconPosition="left"

            type="password"
            name="confirmPassword"
            value={confirmPassword}
            placeholder="Confirm Password"

            onChange={onChange}
            error={errors.confirmPassword}
          />

          <Button
            color="teal"
            fluid={true}
            size="large"
            onClick={preventDefault(signup)}
          >
            Signup
          </Button>
        </Form>
      </Segment>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  signup: (payload: Payloads.Signup) => dispatch(Actions.signup(payload))
});

export const SignUpForm = connect(null, mapDispatchToProps)(Component);