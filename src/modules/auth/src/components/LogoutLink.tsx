import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Link, LinkProps } from 'react-router-dom';

import { Actions } from '../..';
import { preventDefault } from '../../../common';

interface Props extends LinkProps {
  logout: () => any;
  to: string;
}

const Component: React.SFC<Props> = ({ children, logout, ...props }) => (
  <Link {...props} onClick={preventDefault(logout)}>{children}</Link>
);

const mapStateToProps = () => ({ to: '/logout' });

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  logout: () => dispatch(Actions.logout())
});

export const LogoutLink = connect(mapStateToProps, mapDispatchToProps)(Component);