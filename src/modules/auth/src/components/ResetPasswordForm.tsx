import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Button, Form, Segment } from 'semantic-ui-react';
import { RouteComponentProps, withRouter } from 'react-router';

import { Actions, Payloads, Results, selector } from '../..';
import { Errors, getErrors, isEmail, isEmpty, preventDefault, InputWrapper } from '../../../common';

interface Props extends RouteComponentProps<any> {
  resetPassword: (user: Payloads.ResetPassword) => any;
  checkResetToken: (token: string) => any;
  checkResetTokenResult: Results.CheckResetToken;
}

type User = {
  token: string;
  email: string;
  password: string;
  confirmPassword: string;
};

type State = {
  user: User,
  errors: Errors<User>;
};

class Component extends React.Component<Props, State> {
  state: State = {
    user: {
      email: '',
      password: '',
      confirmPassword: '',
      token: '',
    },
    errors: {},
  };

  private onChange = ((key: string, value: string) => {
    const { user } = this.state;

    user[key] = value;

    this.setState({ user });
  });

  private checkResetToken = (() => {
    const { checkResetToken, match: { params } } = this.props;

    checkResetToken(params.token);
  });

  private resetPassword = (() => {
    const { formIsValid, state: { user }, props: { resetPassword } } = this;

    if (formIsValid()) {
      resetPassword(user);
    }
  });

  private formIsValid = (() => {
    const { validator, state: { user } } = this;
    const errors = getErrors(user, validator);

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  });

  private validator = ((key: string, value: string, data: any) => {
    if (key === 'email') {
      if (isEmpty(value)) {
        return 'Email field is required.';
      } else if (!isEmail(value)) {
        return 'Email is not valid.';
      }

    } else if (key === 'password') {
      if (isEmpty(value)) {
        return 'Password field is required.';
      }

    } else if (key === 'confirmPassword') {
      if (isEmpty(value)) {
        return 'Please confirm the password.';
      } else if (data.password && data.password !== value) {
        return 'Wrong password.';
      }
    }

    return '';
  });

  componentWillReceiveProps(props: Props) {
    const { token, email } = props.checkResetTokenResult;
    this.setState({ user: { token, email } } as any);
  }

  componentWillMount() {
    this.checkResetToken();
  }

  render() {
    const { resetPassword, onChange } = this;
    const { errors, user: { email, password, confirmPassword } } = this.state;

    return (
      <Segment>
        <Form
          size="large"
          onSubmit={preventDefault(resetPassword)}
        >
          <InputWrapper
            as={Form.Input}

            icon="envelope"
            iconPosition="left"

            type="email"
            name="email"
            value={email}
            placeholder="Email"

            disabled={true}
            onChange={onChange}
            error={errors.email}
          />

          <InputWrapper
            as={Form.Input}

            icon="lock"
            iconPosition="left"

            type="password"
            name="password"
            value={password}
            placeholder="New password"

            onChange={onChange}
            error={errors.password}
          />

          <InputWrapper
            as={Form.Input}

            icon="lock"
            iconPosition="left"

            type="password"
            name="confirmPassword"
            value={confirmPassword}
            placeholder="Confirm password"

            onChange={onChange}
            error={errors.confirmPassword}
          />

          <Button
            color="teal"
            fluid={true}
            onClick={preventDefault(resetPassword)}
          >
            Save Password
          </Button>
        </Form>
      </Segment>
    );
  }
}

const mapStateToProps = (state: any) => ({
  checkResetTokenResult: selector(state).checkResetTokenResult
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  resetPassword: (user: Payloads.ResetPassword) => dispatch(Actions.resetPassword(user)),
  checkResetToken: (token: string) => dispatch(Actions.checkResetToken(token)),
});

export const ResetPasswordForm = withRouter(connect(mapStateToProps, mapDispatchToProps)(Component));