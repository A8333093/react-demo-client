import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect, Dispatch } from 'react-redux';
import { Button, Form, Message, Segment } from 'semantic-ui-react';

import { Actions } from '../..';
import { Errors, getErrors, isEmail, isEmpty, preventDefault, InputWrapper } from '../../../common';

interface Props {
  forgotPassword: (email: string) => any;
}

type State = {
  email: string;
  errors: Errors<{ email: string }>;
};

class Component extends React.Component<Props, State> {
  state: State = {
    email: '',
    errors: {},
  };

  private onChange = ((key: string, value: string) => {
    this.setState({ email: value });
  });

  private resetPassword = (() => {
    const { formIsValid, props: { forgotPassword }, state: { email } } = this;

    if (formIsValid()) {
      forgotPassword(email);
    }
  });

  private formIsValid = (() => {
    const { validator, state } = this;
    const errors = getErrors(state, validator);

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  });

  private validator = ((key: string, value: string) => {
    if (key === 'email') {
      if (isEmpty(value)) {
        return 'Email is required.';
      } else if (!isEmail(value)) {
        return 'Email is not valid.';
      }
    }

    return '';
  });

  render() {
    const { onChange, resetPassword } = this;
    const { email, errors } = this.state;

    return (
      <Segment>
        <Form
          size="large"
          onSubmit={preventDefault(resetPassword)}
        >
          <InputWrapper
            as={Form.Input}

            icon="envelope"
            iconPosition="left"

            type="email"
            name="email"
            value={email}
            placeholder="Email"

            onChange={onChange}
            error={errors.email}
          />

          <Button
            color="teal"
            fluid={true}
            size="large"
            onClick={preventDefault(resetPassword)}
          >
            Reset Password
          </Button>

          <Message>
            Already have an account? <Link to="/login">Login</Link>
          </Message>
        </Form>
      </Segment>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  forgotPassword: (email: string) => dispatch(Actions.forgotPassword(email))
});

export const ForgotPasswordForm = connect(null, mapDispatchToProps)(Component);