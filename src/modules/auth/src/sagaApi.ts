import { get, post } from '../../ajax';

const login = <U>(user: U) => post('/login', user);
const logout = () => get('/logout');
const signup = <U>(user: U) => post('/sign-up', user);
const activate = (token: string) => get(`/activate/${token}`);
const currentUser = () => get('/current-user');
const resetPassword = <U>(user: U) => post('/password-reset', user);
const forgotPassword = (email: string) => post('/password-forgot', { email });
const activateAccount = (token: string) => get(`/activate/${token}`);
const checkResetToken = (token: string) => get(`/password-reset/${token}`);

export const sagaApi = {
  login,
  logout,
  signup,
  activate,
  currentUser,
  resetPassword,
  forgotPassword,
  activateAccount,
  checkResetToken,
};