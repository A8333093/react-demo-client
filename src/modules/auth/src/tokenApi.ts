import * as Cookie from 'js-cookie';

import { JWT_TOKEN } from '..';

const getToken = () => Cookie.get(JWT_TOKEN);
const saveToken = (token: string) => Cookie.set(JWT_TOKEN, token);
const cleanToken = () => Cookie.remove(JWT_TOKEN);

export const TokenApi = {
  getToken,
  saveToken,
  cleanToken,
};