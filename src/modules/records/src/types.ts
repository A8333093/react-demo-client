import { Record } from '..';

type SortBy = string;

type Id = string;

// Payloads
export namespace Payloads {
  export type Save = Record;

  export type Fetch = SortBy;

  export type Remove = Id;
}

// Results
export namespace Results {
  export type Save = Record ;

  export type Fetch = Record[];

  export type Remove = undefined;
}