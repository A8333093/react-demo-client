import { moduleId, State } from '..';

export const selector = <S>(state: S): State => state[moduleId];