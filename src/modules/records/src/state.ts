import { Record } from './models';

export interface State {
  list?: Record[];
  current?: Record;
}