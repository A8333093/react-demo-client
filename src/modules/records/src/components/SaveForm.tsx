import * as React from 'react';
import { Form, Segment } from 'semantic-ui-react';

import { Record } from '../models';
import Config from '../../../../config';
import { Category } from '../../../categories';
import { DatePicker, Errors, InputWrapper, preventDefault } from '../../../common';

interface Props {
  record: Record;
  categories: Category[];
  errors: Errors<Record>;
  onChange: (name: string, value: string) => any;
}

export const SaveForm: React.SFC<Props> = ({ record, categories, errors, onChange }) => (
  <Segment>
    <Form size="large" onSubmit={preventDefault()}>
      <DatePicker
        name="date"
        onChange={onChange}
        selected={record.date}
        dateFormat={Config.format.date}
      />

      <InputWrapper
        as={Form.Input}
        name="cost"
        value={record.cost && `${record.cost}`}
        placeholder="Cost"

        onChange={onChange}
        error={errors.cost}
      />

      <InputWrapper
        as={Form.Dropdown}
        name="categoryId"
        value={record.categoryId}
        placeholder="Select category"

        search={true}
        selection={true}
        options={categories.map(({ _id: value, title: text }) =>
          ({ key: value, value, text })
        )}

        onChange={onChange}
        error={errors.categoryId}
      />

      <InputWrapper
        as={Form.Input}
        name="note"
        value={record.note}
        placeholder="Note"

        onChange={onChange}
        error={errors.note}
      />
    </Form>
  </Segment>
);