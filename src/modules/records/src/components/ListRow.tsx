import * as React from 'react';
import * as Moment from 'moment';
import { Link } from 'react-router-dom';
import { Icon, Table } from 'semantic-ui-react';

import { Record } from '../..';
import Config from '../../../../config';
import { Category } from '../../../categories';
import { preventDefault } from '../../../common';

interface Props {
  record: Record;
  category: Category;
  onEdit: (record: Record) => any;
  onRemove: (record: Record) => any;
}

export const ListRow: React.SFC<Props> = ({ record, category, onEdit, onRemove }) => {
  const { date, cost, note } = record;
  return (
    <Table.Row>
      <Table.Cell>{Moment(date).format(Config.format.date)}</Table.Cell>
      <Table.Cell>{cost}</Table.Cell>
      <Table.Cell>{category ? category.title : ''}</Table.Cell>
      <Table.Cell>{note}</Table.Cell>
      <Table.Cell>
        <Link to="" onClick={preventDefault(onEdit, record)}>
          <Icon name="edit" /> Edit
        </Link>
      </Table.Cell>
      <Table.Cell>
        <Link to="" onClick={preventDefault(onRemove, record)}>
          <Icon name="remove" /> Delete
        </Link>
      </Table.Cell>
    </Table.Row>
  );
};