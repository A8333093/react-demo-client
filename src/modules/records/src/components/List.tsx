import * as React from 'react';
import { Table } from 'semantic-ui-react';

import { ListRow, Record } from '../..';
import { Category } from '../../../categories';

interface Props {
  records: Record[];
  categories: Category[];
  onEdit: (record: Record) => any;
  onRemove: (record: Record) => any;
}

export const List: React.SFC<Props> = ({ records, categories, onEdit, onRemove }) => (
  <Table celled={true}>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Date</Table.HeaderCell>
        <Table.HeaderCell>Cost</Table.HeaderCell>
        <Table.HeaderCell>Category</Table.HeaderCell>
        <Table.HeaderCell>Note</Table.HeaderCell>
        <Table.HeaderCell width={2} />
        <Table.HeaderCell width={2} />
      </Table.Row>
    </Table.Header>

    {records && records.length > 0 && (
      <Table.Body>
        {records.map(record => {
          const category = categories
              .find(({ _id }) => record.categoryId === _id) as Category;

          return (
            <ListRow key={record._id} {...{ record, category, onEdit, onRemove }} />
          );
        })}
      </Table.Body>
    )}
  </Table>
);