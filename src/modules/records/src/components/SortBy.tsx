import * as React from 'react';
import { Divider, Form } from 'semantic-ui-react';

import { InputWrapper } from '../../../common';

interface Props {
  onSort: (sortBy: string) => any;
}

const option = (key: string, text: string) => ({ key, value: key, text });

export class SortBy extends React.Component<Props> {
  static options = [
    option('date', 'Date'),
    option('categoryId', 'Category'),
    option('cost', 'Cost'),
  ];

  static get default() {
    return SortBy.options[0];
  }

  render() {
    const { onSort } = this.props;

    return (
      <div>
        <InputWrapper
          as={Form.Dropdown}
          name="sortBy"
          value={SortBy.default.value}
          placeholder="Sort by:"

          selection={true}

          onChange={(n, sortBy) => onSort(sortBy)}
          options={SortBy.options}
        />

        <Divider />
      </div>
    );
  }
}

/*

 */