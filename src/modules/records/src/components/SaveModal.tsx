import * as React from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';

import { Record, SaveForm } from '../..';
import { Category } from '../../../categories';
import { Errors, preventDefault } from '../../../common';

interface Props {
  open: boolean;
  record: Record;
  categories: Category[];
  errors: Errors<Record>;
  onChange: (name: string, value: string) => any;
  onSave: () => any;
  onCancel: () => any;
}

export const SaveModal: React.SFC<Props> = ({ open, record, categories, onChange, errors, onCancel, onSave }) => (
  <Modal open={open} onClose={onCancel} basic={true} size="small">
    <Header icon="add" content={`${record._id ? 'Edit' : 'Add new'} record`} />
    <Modal.Content>
      <SaveForm record={record} categories={categories} onChange={onChange} errors={errors} />
    </Modal.Content>
    <Modal.Actions>
      <Button basic={true} color="red" inverted={true} onClick={preventDefault(onCancel)}>
        <Icon name="remove" /> Cancel
      </Button>
      <Button color="green" inverted={true} onClick={preventDefault(onSave)}>
        <Icon name="checkmark" /> Save
      </Button>
    </Modal.Actions>
  </Modal>
);