import * as React from 'react';
import * as Moment from 'moment';
import { connect, Dispatch } from 'react-redux';
import { Button, Container } from 'semantic-ui-react';

import * as Categories from '../../../categories';
import { Actions, List, Record, SaveModal, selector, SortBy } from '../..';
import { Confirm, Errors, getErrors, isEmpty, preventDefault } from '../../../common';

interface Props {
  records: Record[];
  categories: Categories.Category[];
  fetch: {
    records: (sortBy: string) => any;
    categories: () => any;
  };
  save: (record: Record) => any;
  remove: (id: string) => any;
}

type State = {
  toSave?: Partial<Record>;
  toRemove?: Record;
  errors: Errors<Record>;
};

class Component extends React.Component<Props, State> {
  state = {
    toSave: undefined as any as Record,
    toRemove: undefined as any as Record,
    errors: {},
  };

  private onEdit = ((toSave: Record) => this.setState({ toSave }));

  private onRemove = ((toRemove: Record) => this.setState({ toRemove }));

  private onNo = (() => this.setState({ toRemove: undefined }));

  private onYes = (() => {
    const { props: { remove }, state: { toRemove } } = this;

    if (toRemove) {
      remove(toRemove._id);
    }

    this.setState({ toRemove: undefined });
  });

  private onAdd = (() => {
    this.setState({
      toSave: {
        date: Moment().toISOString(),
        cost: null as any,
        categoryId: '',
        note: '',
      }
    });
  });

  private onChange = ((name: string, value: string) => {
    this.state.toSave[name] = value;
  });

  private onCancel = (() => this.setState({
    toSave: undefined,
    errors: {},
  }));

  private onSave = (() => {
    const { props: { save }, state: { toSave }, formIsValid } = this;

    if (toSave && formIsValid()) {
      save(toSave);
      this.setState({
        toSave: undefined,
        errors: {},
      });
    }
  });

  private onSort = ((sortBy: string) => this.props.fetch.records(sortBy));

  private formIsValid = (() => {
    const { validator, state: { toSave } } = this;
    const errors = getErrors(toSave, validator);

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  });

  private validator = ((key: string, value: string) => {
    if (key === 'categoryId') {
      if (isEmpty(value)) {
        return 'Category field is required.';
      }
    }
    if (key === 'cost') {
      if (isEmpty(value)) {
        return 'Cost field is required.';
      }
    }
    if (key === 'note') {
      if (isEmpty(value)) {
        return 'Note field is required.';
      }
    }
    return '';
  });

  componentWillMount() {
    const { categories, records } = this.props.fetch;

    categories();
    records(SortBy.default.key);
  }

  render() {
    const { records, categories } = this.props;
    const { toSave, toRemove, errors } = this.state;
    const { onEdit, onRemove, onNo, onYes, onAdd, onChange, onCancel, onSave, onSort } = this;

    return (
      <Container>
        <SortBy onSort={onSort} />

        <Button onClick={preventDefault(onAdd)}>
          Add new Record
        </Button>

        <List {...{ records, categories, onEdit, onRemove }} />

        {!!toSave && (
          <SaveModal
            open={!!toSave}
            record={toSave}
            categories={categories}
            errors={errors}
            {...{ onChange, onCancel, onSave }}
          />
        )}

        {!!toRemove && (
          <Confirm
            open={!!toRemove}
            {...{ onNo, onYes }}
            title="Are you sure?"
            message={`Remove record "${toRemove.note}" ?`}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  records: selector(state).list,
  categories: Categories.selector(state).list,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  fetch: {
    records: (sortBy: string) => dispatch(Actions.fetch(sortBy)),
    categories: () => dispatch(Categories.Actions.fetch()),
  },
  save: (record: Record) => dispatch(Actions.save(record)),
  remove: (id: string) => dispatch(Actions.remove(id)),
});

export const GrudGrid = connect(mapStateToProps, mapDispatchToProps)(Component);