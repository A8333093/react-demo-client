import * as React from 'react';
import { connect, Dispatch } from 'react-redux';

import { selector } from '../..';

interface Props {
}

class Component extends React.Component<Props> {
  render() {
    return null;
  }
}

const mapStateToProps = (state: any) => selector(state);

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({});

export const View = connect(mapStateToProps, mapDispatchToProps)(Component);