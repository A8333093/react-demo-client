import { reducerBuilder } from '../../common';
import { Actions as A, Record, State } from '..';

const INITIAL_STATE: State = {
  list: [],
  current: undefined,
};

const remove = (id: string, list: Record[] = []): Record[] =>
  list.filter(record => record._id !== id);

const save = (record: Record, list: Record[] = []): Record[] => {
  let isNew = true;
  list.map(_record => {
    if (_record._id === record._id) {
      isNew = false;
      _record = record;
    }

    return _record;
  });

  if (isNew) {
    return [...list, record];
  }

  return list;
};

export const reducer = reducerBuilder(INITIAL_STATE)
  .case(A.fetch.success, (state, { result: list }) => ({ ...state, list }))
  .case(A.save.success, (state, { result: record }) => ({ ...state, list: save(record, state.list) }))
  .case(A.remove.success, (state, { payload: id }) => ({ ...state, list: remove(id, state.list) }));