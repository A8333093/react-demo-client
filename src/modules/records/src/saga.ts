import * as Toastr from 'toastr';
import { all } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { bindAjaxAction } from '../../ajax';
import { Actions as A, sagaApi as Api } from '..';
import { Success, takeEveryAction } from '../../common';

const save = bindAjaxAction(A.save)(
  Api.save,
  ({ payload }: Success<any, any>) => {
    if (payload._id) {
      Toastr.success('Record was updated successfully!');
    } else {
      Toastr.success('Record was created successfully!');
    }
  }
);

const fetch = bindAjaxAction(A.fetch)(Api.fetch);

const remove = bindAjaxAction(A.remove)(
  Api.remove,
  () => Toastr.success('Record was deleted successfully!')
);

export const saga = function* (): SagaIterator {
  yield all([
    takeEveryAction(A.save, save),
    takeEveryAction(A.fetch, fetch),
    takeEveryAction(A.remove, remove),
  ]);
};