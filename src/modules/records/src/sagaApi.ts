import { Record } from '..';
import { Delete, get, post } from '../../ajax';

const save = (record: Record) => post('/record', { record });
const fetch = (sortBy: string) => get(`/records?sortBy=${sortBy}`);
const remove = (id: string) => Delete(`/record/${id}`);

export const sagaApi = {
  save,
  fetch,
  remove,
};