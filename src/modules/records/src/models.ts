import { DbDocument } from '../../common';

export interface Record extends DbDocument {
  id: string;
  userId: string;
  categoryId: string;
  note: string;
  cost: number;
  date: string;
}