import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Divider, Menu, Sidebar, } from 'semantic-ui-react';

import { getFullUserName, User } from '../../modules/users';
import { LogoutLink, selector } from '../../modules/auth';

interface Prop {
  user: User;
  sidebarOpened: boolean;
}

const styles = {
  sidebar: { zIndex: 999999 }
};

const MobileSidebar: React.SFC<Prop> = ({ user, sidebarOpened }) => (
  <Sidebar
    as={Menu}
    animation="uncover"
    inverted={true}
    vertical={true}
    visible={sidebarOpened}
    style={styles.sidebar}
  >
    <Menu.Item as={NavLink} to="/" exact={true}>
      Home
    </Menu.Item>

    <Menu.Item as={NavLink} to="/categories">
      Categories
    </Menu.Item>

    <Menu.Item as={NavLink} to="/records">
      Records
    </Menu.Item>

    <Divider fitted={true} />

    {
      user && user.email
        ? (
          <div>
            <Menu.Item>
              Logged as: {getFullUserName(user)}
            </Menu.Item>
            <Menu.Item as={LogoutLink}>
              Logout
            </Menu.Item>
          </div>
        )
        : (
          <div>
            <Menu.Item as={NavLink} to="/login">
              Log in
            </Menu.Item>

            <Menu.Item as={NavLink} to="/signup">
              Sign Up
            </Menu.Item>
          </div>
        )

    }
  </Sidebar>
);

const mapStateToProps = (state: any) => ({
  user: selector(state).user
});

export default connect(mapStateToProps)(MobileSidebar);