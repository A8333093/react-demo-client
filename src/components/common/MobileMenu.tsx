import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { Button, Icon, Menu, Segment, } from 'semantic-ui-react';
import { getFullUserName, User } from '../../modules/users';
import { LogoutLink, selector } from '../../modules/auth';

interface Props {
  user: User;
  openSidebar: () => any;
}

const styles = {
  signupButton: { marginLeft: '1em' },
};

const MobileMenu: React.SFC<Props> = ({ user, openSidebar }) => (
  <Segment inverted={true} textAlign="center" style={{ padding: '1em 0em' }} vertical={true}>
    <Menu inverted={true} pointing={true} secondary={true} size="large">
      <Menu.Item onClick={openSidebar}>
        <Icon name="sidebar" size="big" />
      </Menu.Item>

      <Menu.Item position="right">
        {
          user && user.email
            ? (
              <span>
                <Button disabled={true} inverted={true}>
                  {getFullUserName(user)}
                </Button>
                &nbsp;
                <Button as={LogoutLink} inverted={true}>
                  Logout
                </Button>
              </span>
            )
            : (
              <span>
                <Button as={NavLink} to="/login" inverted={true}>
                  Log in
                </Button>
                &nbsp;
                <Button
                  as={NavLink}
                  to="/signup"
                  inverted={true}
                  style={styles.signupButton}
                >
                  Sign Up
                </Button>
              </span>
            )
        }
      </Menu.Item>
    </Menu>
  </Segment>
);

const mapStateToProps = (state: any) => ({
  user: selector(state).user
});

export default connect(mapStateToProps)(MobileMenu);