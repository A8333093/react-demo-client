import * as React from 'react';
import { Container, Icon, Segment } from 'semantic-ui-react';

const styles = {
  segment: { margin: '7em 0 0', padding: '2em 0em' },
};

const Bottom: React.SFC = () => (
  <Segment
    inverted={true}
    vertical={true}
    style={styles.segment}
  >
    <Container textAlign="center">
      {/*
        <List horizontal={true} inverted={true} divided={true} link={true}>
          <List.Item as="a" href="#">Site Map</List.Item>
          <List.Item as="a" href="#">Contact Us</List.Item>
          <List.Item as="a" href="#">Terms and Conditions</List.Item>
          <List.Item as="a" href="#">Privacy Policy</List.Item>
        </List>
      */}
      <p><Icon name="cubes" size="huge" /></p>
      <p>{new Date().getFullYear()} <Icon name="copyright" /></p>
    </Container>
  </Segment>
);

export default Bottom;