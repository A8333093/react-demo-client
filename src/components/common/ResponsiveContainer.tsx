import * as React from 'react';
import { Container, Responsive, Sidebar, Visibility, } from 'semantic-ui-react';

import Bottom from './Bottom';
import MobileMenu from './MobileMenu';
import DesktopMenu from './DesktopMenu';
import MobileSidebar from './MobileSidebar';

type DesktopState = {
  fixed: boolean;
};

class DesktopContainer extends React.Component<{}, DesktopState> {
  static containerStyle = { marginTop: '7em' };

  state = { fixed: false };

  private hideFixedMenu = (() => this.setState({ fixed: false }));
  private showFixedMenu = (() => this.setState({ fixed: true }));

  render() {
    const { children } = this.props;
    const { fixed } = this.state;

    return (
      <Responsive minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility once={false} onBottomPassed={this.showFixedMenu} onBottomPassedReverse={this.hideFixedMenu}>
          <DesktopMenu fixed={fixed} />
        </Visibility>

        <Container style={DesktopContainer.containerStyle}>
          {children}
        </Container>

        <Bottom />
      </Responsive>
    );
  }
}

type MobileState = {
  sidebarOpened: boolean;
};

class MobileContainer extends React.Component<{}, MobileState> {
  static containerStyle = { marginTop: '2em' };

  state = { sidebarOpened: false };

  private handlePusherClick = (
    () =>
      this.state.sidebarOpened &&
      this.setState({ sidebarOpened: !this.state.sidebarOpened })
  );

  private handleToggle = (() => this.setState({ sidebarOpened: !this.state.sidebarOpened }));

  render() {
    const { children } = this.props;
    const { sidebarOpened } = this.state;

    return (
      <Responsive {...Responsive.onlyMobile}>
        <Sidebar.Pushable>
          <MobileSidebar sidebarOpened={sidebarOpened} />

          <Sidebar.Pusher dimmed={sidebarOpened} onClick={this.handlePusherClick} style={{ minHeight: '100vh' }}>
            <MobileMenu openSidebar={this.handleToggle} />

            <Container style={MobileContainer.containerStyle}>
              {children}
            </Container>

            <Bottom />
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Responsive>
    );
  }
}

const ResponsiveContainer: React.SFC = ({ children }) => (
  <div>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </div>
);

export default ResponsiveContainer;