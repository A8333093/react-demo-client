import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Button, Icon, Menu, Segment } from 'semantic-ui-react';

import { LogoutLink, selector } from '../../modules/auth';
import { getFullUserName, User } from '../../modules/users';

interface Props {
  user: User;
  fixed: boolean;
}

const styles = {
  segment: { padding: '1em 0em' },
  signupButton: { marginLeft: '1em' },
};

const DesktopMenu: React.SFC<Props> = ({ user, fixed }) => (
  <Segment inverted={true} textAlign="center" style={styles.segment} vertical={true}>
    <Menu
      fixed={fixed ? 'top' : undefined}
      inverted={!fixed}
      pointing={!fixed}
      secondary={!fixed}
      size="large"
    >
      <Menu.Item>
        <Icon name="cubes" size="large" />React Demo
      </Menu.Item>

      <Menu.Item as={NavLink} to="/" exact={true}>
        Home
      </Menu.Item>

      <Menu.Item as={NavLink} to="/categories">
        Categories
      </Menu.Item>

      <Menu.Item as={NavLink} to="/records">
        Records
      </Menu.Item>

      <Menu.Item position="right">
        {
          user && user.email
            ? (
              <span>
                <Button disabled={true} inverted={!fixed}>
                  {getFullUserName(user)}
                </Button>
                &nbsp;
                <Button as={LogoutLink} inverted={!fixed}>
                  Logout
                </Button>
              </span>
            )
            : (
              <span>
                <Button as={NavLink} to="/login" inverted={!fixed}>
                  Log in
                </Button>
                &nbsp;
                <Button
                  as={NavLink}
                  to="/signup"
                  inverted={!fixed}
                  primary={fixed}
                  style={styles.signupButton}
                >
                  Sign Up
                </Button>
              </span>
            )
        }
      </Menu.Item>
    </Menu>
  </Segment>
);

const mapStateToProps = (state: any) => ({
  user: selector(state).user
});

export default connect(mapStateToProps)(DesktopMenu);