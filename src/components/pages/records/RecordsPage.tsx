import * as React from 'react';
import { Header } from 'semantic-ui-react';

import AppPage from '../AppPage';
import * as Records from '../../../modules/records';

const RecordsPage: React.SFC = () => (
  <AppPage title="Records" goOnFailureCheck="/login">
    <Header as="h1">Records</Header>

    <Records.GrudGrid />
  </AppPage>

);

export default RecordsPage;