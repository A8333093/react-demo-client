import * as React from 'react';
import { connect } from 'react-redux';
import { Header, Icon } from 'semantic-ui-react';

import AppPage from './AppPage';
import { getFullUserName, User } from '../../modules/users';
import { AuthorizedBlock, selector } from '../../modules/auth';

interface Props {
  user?: User;
}

const Paragraph = () => (
  <p>
    {[
      'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ',
      'tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas ',
      'semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ',
      'ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean ',
      'fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. ',
      'Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor ',
      'neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, ',
      'accumsan porttitor, facilisis luctus, metus',
    ].join('')}
  </p>
);

const MainPage: React.SFC<Props> = ({ user }) => {
  return (
    <AppPage title="React Demo App">
      <Header as="h1">
        <Icon name="cubes" /> React Demo App
      </Header>

      <AuthorizedBlock>
        <h3>Welcome {getFullUserName(user)}</h3>
        <br />
      </AuthorizedBlock>

      <Paragraph />

      <Paragraph />

      <Paragraph />
    </AppPage>
  );
};

const mapStateToProps = (state: any) => ({
  user: selector(state).user
});

export default connect(mapStateToProps)(MainPage);