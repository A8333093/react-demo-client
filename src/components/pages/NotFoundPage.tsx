import * as React from 'react';
import AppPage from './AppPage';

export default () => (
  <AppPage checkAuthorization={false}>
    <h2>Not found</h2>
    <p>Sorry! The page you are looking for could not be found.</p>
  </AppPage>
);