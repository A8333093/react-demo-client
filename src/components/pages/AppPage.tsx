import * as React from 'react';
import Helmet from 'react-helmet';

import * as Auth from '../../modules/auth';
import { Dimmer } from '../../modules/ajax';
import ResponsiveContainer from '../common/ResponsiveContainer';

interface Props {
  title?: string;
  checkAuthorization?: boolean;
  goLoginOnFailure?: boolean;
  goOnSuccessCheck?: string;
  goOnFailureCheck?: string;
}

const AppPage: React.SFC<Props> =
  ({ children, title, checkAuthorization = true, goLoginOnFailure, goOnSuccessCheck, goOnFailureCheck }) => (
    <div>
      <Helmet title={title} />

      <Dimmer />

      {checkAuthorization && <Auth.CheckAuthorization
        goLoginOnFailure={goLoginOnFailure}
        goOnSuccess={goOnSuccessCheck}
        goOnFailure={goOnFailureCheck}
      />}

      <ResponsiveContainer>
        {children}
      </ResponsiveContainer>
    </div>
  );

export default AppPage;