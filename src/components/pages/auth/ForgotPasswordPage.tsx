import * as React from 'react';
import { Header, Icon } from 'semantic-ui-react';

import AppPage from '../AppPage';
import * as Auth from '../../../modules/auth';

const PasswordForgotPage: React.SFC = () => (
  <AppPage title="Reset Password" goOnSuccessCheck="/">
    <div style={{ maxWidth: '450px', margin: '0 auto' }}>
      <Header as="h1" textAlign="center">
        <Icon name="unlock" /> Reset Password
      </Header>

      <Auth.ForgotPasswordForm />
    </div>
  </AppPage>
);

export default PasswordForgotPage;