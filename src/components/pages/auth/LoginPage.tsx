import * as React from 'react';
import { Link } from 'react-router-dom';
import { Header, Icon, Message } from 'semantic-ui-react';

import AppPage from '../AppPage';
import * as Auth from '../../../modules/auth';

const LoginPage: React.SFC = () => (
  <AppPage title="Login" goOnSuccessCheck="/records">
    <div style={{ maxWidth: '450px', margin: '0 auto' }}>
      <Header as="h1" textAlign="center">
        <Icon name="sign in" /> Log-in to your account
      </Header>

      <Auth.LoginForm />

      <Message>
        <Link to="/password-forgot">Forgot your password?</Link>
        <br />
        Need an account? <Link to="/signup">Signup</Link>
      </Message>
    </div>
  </AppPage>
);

export default LoginPage;