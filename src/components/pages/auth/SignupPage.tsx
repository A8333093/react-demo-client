import * as React from 'react';
import { Link } from 'react-router-dom';
import { Header, Icon, Message } from 'semantic-ui-react';

import AppPage from '../AppPage';
import * as Auth from '../../../modules/auth';

const SignUpPage: React.SFC = () => (
  <AppPage title="Signup" goOnSuccessCheck="/">
    <div style={{ maxWidth: '450px', margin: '0 auto' }}>
      <Header as="h1" textAlign="center">
        <Icon name="sign in" /> Signup
      </Header>

      <Auth.SignUpForm />

      <Message>
        Already have an account? <Link to="/login">Login</Link>
      </Message>
    </div>
  </AppPage>
);

export default SignUpPage;