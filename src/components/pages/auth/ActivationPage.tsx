import * as React from 'react';
import { Header, Icon } from 'semantic-ui-react';

import AppPage from '../AppPage';
import * as Auth from '../../../modules/auth';

const ActivationPage: React.SFC = () => (
  <AppPage title="Account Activation" goOnSuccessCheck="/">
    <div style={{ maxWidth: '450px', margin: '0 auto' }}>
      <Header as="h1" textAlign="center">
        <Icon name="user" /> Activate your account
      </Header>

      <Auth.ActivateUser />
    </div>
  </AppPage>
);

export default ActivationPage;