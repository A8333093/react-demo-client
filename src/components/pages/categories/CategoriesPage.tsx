import * as React from 'react';
import { Header } from 'semantic-ui-react';

import AppPage from '../AppPage';
import * as Categories from '../../../modules/categories';

const CategoriesPage: React.SFC = () => (
  <AppPage title="Categories" goOnFailureCheck="/login">
    <Header as="h1">Categories</Header>

    <Categories.GrudGrid />
  </AppPage>
);

export default CategoriesPage;