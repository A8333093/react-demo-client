import * as React from 'react';
import { RouteProps } from 'react-router';
import { Route, Router, Switch } from 'react-router-dom';

import history from '../services/history';

import '../styles/App.css';

interface Props {
  routes: RouteProps[];
}

const App: React.SFC<Props> = ({ routes }) => (
  <Router history={history}>
    <Switch>
      {routes.map((route, key) => (
        <Route key={key} {...route} />
      ))}
    </Switch>
  </Router>
);

export default App;