import { combineReducers } from 'redux';
import * as Ajax from '../modules/ajax';
import * as Auth from '../modules/auth';
import * as Records from '../modules/records';
import * as Categories from '../modules/categories';

export default combineReducers({
  [Ajax.moduleId]: Ajax.reducer,
  [Auth.moduleId]: Auth.reducer,
  [Records.moduleId]: Records.reducer,
  [Categories.moduleId]: Categories.reducer,
});